package testing;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

import org.junit.Test;

import ant_base.AntEdge;
import ant_base.AntGraph;
import ant_base.AntNode;
import ant_io.GraphReader;
import ant_io.GraphWriter;
import ant_statistics.Statistics;
import ant_trajectories.AntSim;
import ant_trajectories.GroundTruth;

public class GraphReaderTest {

	@Test
	public void test() {
		AntGraph ag = GraphReader.ReadBigGraph("ant1");
		ArrayList<AntNode> nodes = new ArrayList<AntNode>();
		AntNode n = new AntNode(1, 1, 1, 1);
		assertEquals(n.GetOutDegree(), 0);
		nodes.add(n);
		nodes.add(n);
		for (AntNode n2 : nodes)
			n2.IncrementInDegree();
		assertEquals(n.GetInDegree(), 2);

		double average = 0;
		int[] result = Statistics.ExtractNodeProbabilities(ag);
		for (int i = 0; i < result.length; i++) {
			System.out.println(i + " " + result[i]);
			average += (i * result[i]);
		}
		average = average / 101;
		System.out.println("" + average);

		result = Statistics.FindNodeDegrees(ag);
		for (int i = 0; i < result.length; i++)
			System.out.println(i + " " + result[i]);

		GraphWriter gw = new GraphWriter(GraphReader.ReadBigGraph("ant1"));
		try {
			gw.writeGraph("resultGraph.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// GraphWriter2 gw2 = new GraphWriter2(gr.ReadBigGraph("ant1"));
		// try {
		// gw2.writeGraph("resultGraph2.txt");
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		ag = GraphReader.ReadBigGraph("resultGraph.txt");
		ArrayList<AntNode> nodes2 = ag.getBaseNodes();
		for (AntNode n1 : nodes2)
			if (n1.GetInDegree() > 2)
				System.out.println("error");
		AntEdge ae = new AntEdge("1 2");
		assertEquals(ae.GetIncidentFromNodeId(), 1);
		assertEquals(ae.GetIncidentToNodeId(), 2);

	}

	@Test
	public void TestGroundTruth() {
		AntGraph graph = GraphReader.ReadBigGraph("resultGraph.txt");
		graph.computeGroundTruth();
		TreeMap<Integer, ArrayList<AntNode>> gt = graph.getGroundTruthPaths();
		TreeMap<Integer, ArrayList<AntNode>> map = GroundTruth
				.create("resultGraph.txt");
		for (int i = 0; i < 50; i++) {
			for (AntNode n1 : map.get(i))
				System.out.print(n1.nodeId + " ");
			System.out.println();

			for (AntNode n1 : gt.get(i))
				System.out.print(n1.nodeId + " ");
			System.out.println();
		}
	}

	public void CreateEmptyGroundTruthGraph(String outputFilename) {
		AntGraph graph = GraphReader.ReadBigGraph("resultGraph.txt");
		graph.computeGroundTruth();

		try {
			graph.outputGraphWithGroundTruthToFile(outputFilename);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	@Test
	public void OutputGraphForAnalysis() {
		AntGraph graph = GraphReader.ReadBigGraphWithGroundTruth("test2.txt");

		for (int i = 0; i < 400000; i++) {
			graph.performUserRandomWalk((i % 50), 0.2f);
		}

		try {
			graph.outputGraphForAnalysisToFile("test1.txt");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void OutputGraphForAnalysis(int numUsers, float errorChance,
			String inputFile, String outputFile) {
		AntGraph graph = GraphReader.ReadBigGraphWithGroundTruth(inputFile);

		for (int i = 0; i < numUsers; i++)
			graph.performUserRandomWalk((i % 50), errorChance);

		try {
			graph.outputGraphForAnalysisToFile(outputFile);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// Methods to create data
	@Test
	public void NewGraphAndGroundTruth() {
		test();
		CreateEmptyGroundTruthGraph("G5_Empty.txt");
	}


	@Test
	public void CreateAnalysisGraphs() {
		// params
		float[] error = { 0.02f, 0.03f, 0.04f, 0.06f, 0.07f, 0.08f, 0.09f,
				0.1f, 0.11f, 0.12f, 0.13f, 0.14f, 0.16f, 0.17f, 0.18f, 0.19f };

		for (float e : error) {
			int numUsers = 5;
			for (int i = 0; i < 4; i++) {
				numUsers *= 10;
				String params = "_u=" + numUsers + "e=" + e + ".txt";

				// calculations
				OutputGraphForAnalysis(numUsers, e, "G1_Empty.txt", "G1"
						+ params);
				OutputGraphForAnalysis(numUsers, e, "G2_Empty.txt", "G2"
						+ params);
				OutputGraphForAnalysis(numUsers, e, "G3_Empty.txt", "G3"
						+ params);
				OutputGraphForAnalysis(numUsers, e, "G4_Empty.txt", "G4"
						+ params);
				OutputGraphForAnalysis(numUsers, e, "G5_Empty.txt", "G5"
						+ params);
			}
		}
	}

	// more general testing methods

	@Test
	public void TestViz() {
		try {
			GraphReader.BigGraphFileToVizFile("sim2.txt", "viz.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testSimAnt() {
		AntSim a = new AntSim(1, 1);
		a.runSimOutputStrings();
	}
	
	@Test
	public void testSimAntOutputStrings()
	{
		AntSim a = new AntSim(50,100);
		try {
			a.writeFiles(a.runSimOutputStrings());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testFindDistance() {
		double[] result = null;
		double[] result2 = null;
		double sum1 = 0, sum2 = 0;

		try {
			result = Statistics.FindDistanceSim(50);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int i = 0; i < 50; i++) {
			System.out.println((i + 1) + " : " + result[i]);
			sum1 += result[i];
		}
		try {
			result2 = Statistics.FindDistanceGT1(50);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		for (int i = 0; i < 50; i++) {
			System.out.println((i + 1) + " : " + result2[i]);
			sum2 += result2[i];
		}
		Arrays.sort(result);
		Arrays.sort(result2);
		for (int i = 0; i < 50; i++) {
			System.out.println((i + 1) + " : " + (result[i] - result2[i]));
		}
		System.out.println("sim: " + sum1 + " gt: " + sum2);
	}

}
