package testing;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

import ant_base.AntGraph;
import ant_base.AntNode;
import ant_io.GraphReader;
import ant_io.ParseGroundTruth;
import ant_statistics.BoundInclusion;
import ant_statistics.Histogram;
import ant_statistics.Partition;
import ant_statistics.PartitionCell;
import ant_statistics.Statistics;
import ant_trajectories.AntSim;
import ant_mysql.AntState;

public class StatisticsTesting {
	
	String mainDir = "F:\\Users\\Travis\\Documents\\School\\Kobourov\\Fall2013Research\\";
	static String gtColonyGraphDir = "C:\\Dropbox\\Team3-Ants\\data\\graphfiles\\";
	
	
	
	@Test
	public void TestPartitionCell()
	{
		PartitionCell<Integer> ii, ie, ei, ee;
		ii = new PartitionCell<Integer>(1,3, BoundInclusion.IncludeInclude);
		ie = new PartitionCell<Integer>(1,3, BoundInclusion.IncludeExclude);
		ei = new PartitionCell<Integer>(1,3, BoundInclusion.ExcludeInclude);
		ee = new PartitionCell<Integer>(1,3, BoundInclusion.ExcludeExclude);

		assertFalse(ii.checkMembershipOf(0));
		assertTrue(ii.checkMembershipOf(1));
		assertTrue(ii.checkMembershipOf(2));
		assertTrue(ii.checkMembershipOf(3));
		assertFalse(ii.checkMembershipOf(4));

		assertFalse(ie.checkMembershipOf(0));
		assertTrue(ie.checkMembershipOf(1));
		assertTrue(ie.checkMembershipOf(2));
		assertFalse(ie.checkMembershipOf(3));
		assertFalse(ie.checkMembershipOf(4));

		assertFalse(ei.checkMembershipOf(0));
		assertFalse(ei.checkMembershipOf(1));
		assertTrue(ei.checkMembershipOf(2));
		assertTrue(ei.checkMembershipOf(3));
		assertFalse(ei.checkMembershipOf(4));

		assertFalse(ee.checkMembershipOf(0));
		assertFalse(ee.checkMembershipOf(1));
		assertTrue(ee.checkMembershipOf(2));
		assertFalse(ee.checkMembershipOf(3));
		assertFalse(ee.checkMembershipOf(4));
	}
	
	@Test
	public void TestPartitionAndHistogram() throws Exception
	{
		// vars
		int numCells = 100;
		TreeMap<Integer, AntState[]> c5 = ParseGroundTruth.getColony05();
		
		
		double[] dist = Statistics.getColonyDeltaDistances(c5);
		Partition P = new Partition(dist, numCells);
		Histogram hist = new Histogram(P);
		
		System.out.println("mean: " + hist.mean);
		System.out.println("median: " + hist.median);
		System.out.println("median unique: " + hist.medianIsUnique);
		System.out.println("mode index: " + hist.modeIndex);
		System.out.println("unimodal: " + hist.isUnimodal);
		System.out.println("population: " + hist.populationSize);
		
		System.out.println(hist.toBarPlotMatlabString(
				"data", "X", "mean distance", "ant count", "Simulated Colonies"));
	}

	@Test
	public void TestGetColonyDeltaDistances() throws Exception
	{
		String outFile = mainDir + "GT_ColonyDeltaDist.txt";
		System.out.println(outFile);
		int splits = 200;
		
		double[][] colonyDeltas = new double[3][];
		Histogram[] hists = new Histogram[3];
		TreeMap<Integer, AntState[]>[] temp = new TreeMap[2];
		temp[0] = ParseGroundTruth.getColony05();
		temp[1] = ParseGroundTruth.getColony06();
		TreeMap<Integer, AntState[]> c5 = ParseGroundTruth.getColony05();
		TreeMap<Integer, AntState[]> c6 = ParseGroundTruth.getColony06();
		
		// the real deal
		TreeMap<Integer, AntState[]> gt = ParseGroundTruth.combineColonies(temp);
		
		colonyDeltas[0] = Statistics.getColonyDeltaDistances(c5);
		colonyDeltas[1] = Statistics.getColonyDeltaDistances(c6);
		colonyDeltas[2] = Statistics.getColonyDeltaDistances(gt);
		
		assertTrue(colonyDeltas[0].length == c5.size() * 100);
		assertTrue(colonyDeltas[1].length == c6.size() * 100);
		assertTrue(colonyDeltas[2].length == gt.size() * 100);
		
		hists[0] = new Histogram(new Partition(colonyDeltas[0], splits));
		hists[1] = new Histogram(new Partition(colonyDeltas[1], splits));
		hists[2] = new Histogram(new Partition(colonyDeltas[2], splits));
		
		Formatter writer = null;
		writer = new Formatter(outFile);
		writer.format(hists[0].toBarPlotMatlabString("C5", "X_1", "delta distance", "ant count", "Colony 5"));
		writer.format(hists[1].toBarPlotMatlabString("C6", "X_2", "delta distance", "ant count", "Colony 6"));
		writer.format(hists[2].toBarPlotMatlabString("C5C6", "X_3", "delta distance", "ant count", "Colony 5 & 6 together"));
		
		writer.close();
	}
	
	@Test
	public void TestGetColonyDeltaTurnAmounts() throws Exception
	{
		String outFile = mainDir + "GT_ColonyDeltaAngle.txt";
		System.out.println(outFile);
		int splits = 200;
		
		double[][] colonyDeltas = new double[3][];
		Histogram[] hists = new Histogram[3];
		TreeMap<Integer, AntState[]>[] temp = new TreeMap[2];
		temp[0] = ParseGroundTruth.getColony05();
		temp[1] = ParseGroundTruth.getColony06();
		TreeMap<Integer, AntState[]> c5 = ParseGroundTruth.getColony05();
		TreeMap<Integer, AntState[]> c6 = ParseGroundTruth.getColony06();
		
		// the real deal
		TreeMap<Integer, AntState[]> gt = ParseGroundTruth.combineColonies(temp);
		
		colonyDeltas[0] = Statistics.getColonyDeltaDegrees(c5);
		colonyDeltas[1] = Statistics.getColonyDeltaDegrees(c6);
		colonyDeltas[2] = Statistics.getColonyDeltaDegrees(gt);
		
		assertTrue(colonyDeltas[0].length == c5.size() * 99);
		assertTrue(colonyDeltas[1].length == c6.size() * 99);
		assertTrue(colonyDeltas[2].length == gt.size() * 99);
		
		hists[0] = new Histogram(new Partition(colonyDeltas[0], splits));
		hists[1] = new Histogram(new Partition(colonyDeltas[1], splits));
		hists[2] = new Histogram(new Partition(colonyDeltas[2], splits));
		
		Formatter writer = null;
		writer = new Formatter(outFile);
		writer.format(hists[0].toBarPlotMatlabString("C5", "X_1", "delta angle", "ant count", "Colony 5"));
		writer.format(hists[1].toBarPlotMatlabString("C6", "X_2", "delta angle", "ant count", "Colony 6"));
		writer.format(hists[2].toBarPlotMatlabString("C5C6", "X_3", "delta angle", "ant count", "Colony 5 & 6 together"));
		
		writer.close();
	}
	
	@Test
	public void TestGetColonyDeltaTurnAmountsAndDistancesSynthetic() throws Exception
	{
		String outFile = mainDir + "GT_SyntheticDeltas.txt";
		System.out.println(outFile);
		int splits = 200;
		int runs = 100;
		// one for angle and distance
		double[][] colonyDeltas = new double[2][];
		Histogram[] hists = new Histogram[2];
		TreeMap<Integer, AntState[]> allColonies[] = new TreeMap[runs];
		TreeMap<Integer, AntState[]> combined;
		// the model
		AntSim as = null;
		
		// run simulations
		for(int i = 0; i < runs; i++)
		{
			as = new AntSim(50, 100);
			allColonies[i] = as.runSimOutputStrings();
		}
		
		combined = ParseGroundTruth.combineColonies(allColonies);
		
		colonyDeltas[0] = Statistics.getColonyDeltaDistances(combined);
		colonyDeltas[1] = Statistics.getColonyDeltaDegrees(combined);
		
		assertTrue(colonyDeltas[0].length == combined.size() * 100);
		assertTrue(colonyDeltas[1].length == combined.size() * 99);
		
		hists[0] = new Histogram(new Partition(colonyDeltas[0], splits));
		hists[1] = new Histogram(new Partition(colonyDeltas[1], splits));
		
		Formatter writer = null;
		writer = new Formatter(outFile);
		writer.format(hists[0].toBarPlotMatlabString("C5", "X_1", "delta distances", "ant count", "Centripetal Colony"));
		writer.format(hists[1].toBarPlotMatlabString("C6", "X_2", "delta angle", "ant count", "Centripetal Colony"));
		
		writer.close();
	}
	
	@Test
	public void TestExtractColonyMetricsSynthetic() throws Exception
	{
		String outFile = mainDir + "SyntheticMetrics.txt";
		System.out.println(outFile);
		int splits = 100;
		int runs = 10;
		TreeMap<Integer, AntState[]> allColonies[] = new TreeMap[runs];
		TreeMap<Integer, AntState[]> combined;
		// the model
		AntSim as = null;
		
		// run simulations
		for(int i = 0; i < runs; i++)
		{
			as = new AntSim(50, 100);
			allColonies[i] = as.runSimOutputStrings();
		}
		
		combined = ParseGroundTruth.combineColonies(allColonies);
		
		Formatter writer = null;
		writer = new Formatter(outFile);
		writer.format(Statistics.extractColonyMetrics(combined, splits, "Synthetic"));
		writer.close();
	}
	
	@Test
	public void TestExtractColonyMetricsReal() throws Exception
	{
		String outFile = mainDir + "RealMetrics.txt";
		System.out.println(outFile);
		int splits = 100;
		TreeMap<Integer, AntState[]> allColonies[] = new TreeMap[2];
		allColonies[0] = ParseGroundTruth.getColony05();
		allColonies[1] = ParseGroundTruth.getColony06();
		TreeMap<Integer, AntState[]> combined = ParseGroundTruth.combineColonies(allColonies);
		
		Formatter writer = null;
		writer = new Formatter(outFile);
		writer.format(Statistics.extractColonyMetrics(combined, splits, "Colony 5&6"));
		writer.close();
	}
	
	@Test
	public void TestRunNewRandomUserWalks() throws FileNotFoundException
	{
		// vars
		int counter;
		double[] eChoice = {0.1f, 0.2f, 0.3f};
		double[] eUser = {5,10,15,25,35};
		String baseInputFile = mainDir + "groundtruth1\\5Dec2013\\5Dec2013_sim";
		String baseOutputFile = mainDir + "groundtruth1\\5Dec2013\\5Dec2013_sim";
		
		for(int i = 1; i < 6; i++)
		{
			String inFile = baseInputFile + i + ".txt";
			for(double eC : eChoice)
			{
				String eCstr = String.format("eC=%f", eC);
				for(double eU : eUser)
				{
					counter = 0;
					String eUstr = String.format("_eU=%f.txt", eU);
					String outFile = baseOutputFile + i + eCstr + eUstr;
					AntNode[][] writeBack = OutputGraphForAnalysis(200, eC, eU, inFile, outFile);
				}
			}
		}
	}
	
	private AntNode[][] OutputGraphForAnalysis(
			int numUsers, 
			double choiceErrorChance, 
			double userAccuracy,
			String inputFile, 
			String outputFile) throws FileNotFoundException
	{
		AntNode[][] ret = new AntNode[numUsers][];
		AntGraph graph = GraphReader.ReadBigGraph(inputFile);
		graph.computeGroundTruth();
		int counter = 1;
		
		for(int i = 0; i < numUsers; i++)
		{
			int antId = (i % 50);
			ret[i] = graph.performUserRandomWalk(antId, choiceErrorChance, userAccuracy);
			AntGraph.writeAntPathToFile(outputFile + "_" + antId + "_" + counter++ + ".txt", antId, -1, ret[i]);
		}
		
		try {
		graph.outputGraphForAnalysisToFile(outputFile);
		} catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		System.out.println(outputFile + "is done.\n");
		return ret;
	}
	
	@Test
	public void getCliqueColonyPersistenceTest()
	{
		AntGraph g = GraphReader.ReadBigGraph(gtColonyGraphDir + "gt2.txt");
		int[][] occurences = Statistics.getColonyCliquePersistence(g);
		
		for(int i = 0; i < occurences.length; i++)
		{
			System.out.println(String.format("Clique size: %d", i));
			for(int j = 0; j < occurences[i].length; j++)
			{
				int val = occurences[i][j];
				if(val != 0)
					System.out.println(String.format("duration %d: %d", i, val));
				
			}
		}
	}
}
