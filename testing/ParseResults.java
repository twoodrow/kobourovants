package testing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;

import org.junit.Test;

import ant_mysql.AntState;
import ant_statistics.Statistics;
import ant_trajectories.AntSim;

public class ParseResults {

	// vars
	ArrayList<Double> vals = new ArrayList<Double>();
	
	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		String root = "C:\\Dropbox\\Team3-Ants\\src_ext\\results";
		String error = "e=0.04";
		String users = "_u=5000";
		
		ParseResults pr = new ParseResults();
		
		pr.parseValue(root+"\\"+"\\"+"ResultsG1"+users+error+".txt");
		pr.parseValue(root+"\\"+"\\"+"ResultsG2"+users+error+".txt");
		pr.parseValue(root+"\\"+"\\"+"ResultsG3"+users+error+".txt");
		pr.parseValue(root+"\\"+"\\"+"ResultsG4"+users+error+".txt");
		pr.parseValue(root+"\\"+"\\"+"ResultsG5"+users+error+".txt");
		
		pr.outputAverage();
	}
	
	public void parseValue(String filename) throws FileNotFoundException
	{
		Scanner reader = new Scanner(new FileInputStream(filename));
		
		reader.next();
		reader.next();
		reader.next();
		reader.next();
		reader.next();
		reader.next();
		reader.next();
		vals.add(reader.nextDouble());
		
		reader.close();
	}
	
	public void outputAverage()
	{
		double sum = 0;
		
		for(int i = 0; i < vals.size(); i++)
			sum += vals.get(i);
		
		System.out.print(sum / vals.size());
	}
	
	public float getAverage()
	{
		float sum = 0;
		
		for(int i = 0; i < vals.size(); i++)
			sum += vals.get(i);
		
		return sum / vals.size();
	}
	
	@Test
	public void outputAntSimulatorData()
	{
		//while(true)
		{
			AntSim as = new AntSim(50, 100);
			TreeMap<Integer, AntState[]> colony = as.runSimOutputStrings();
			double[] turn = Statistics.getColonyDeltaDegrees(colony);
			double[] dist = Statistics.getColonyDeltaDistances(colony);
			
			System.out.println("Colony turn total: " + turn[0]);
			System.out.println("Colony dist total: " + dist[0]);
			for(int i = 1; i < 51; i++)
				System.out.println("Ant"+i+"--turn:"+ turn[i] +"\tdist:"+ dist[i]);
		}
		
	}

}
