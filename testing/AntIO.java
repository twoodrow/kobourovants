package testing;

import org.junit.Test;

import ant_io.GroundTruthFileExtractor;

public class AntIO {

	@Test
	public void testExtractAntGroundTruth() {
		for (int i = 1; i <= 33; i++) {
			String out = "GT_ant" + i + ".txt";
			if (i < 10)
				out = "GT_ant0" + i + ".txt";

			GroundTruthFileExtractor.extractByAntId(
					"colony06-b_10k-GT-all.txt", out, i, 2, 100);
		}
	}

}
