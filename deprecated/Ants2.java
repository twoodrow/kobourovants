package deprecated;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

@Deprecated
public class Ants2 {

	private class Node {

		@SuppressWarnings("unused")
		private double xPos;
		@SuppressWarnings("unused")
		private double yPos;
		private int time;

		public Node(String line) {
			String[] values = line.split("\\s");
			time = Integer.valueOf(values[0]);
			xPos = Double.valueOf(values[1]);
			yPos = Double.valueOf(values[2]);
		}
	}

	private ArrayList<Node> nodes = new ArrayList<Node>();
	private int[] numAnts = new int[101];

	public Ants2() {

	}

	public void readInput(String fileName) throws IOException {

		Scanner scanner = new Scanner(new FileInputStream(fileName));
		if (!scanner.hasNext()) {
			System.out.println("Input file is empty.");
			scanner.close();
			System.exit(1);
		}

		scanner.nextLine(); // first line is ignored
		while (scanner.hasNextLine()) {
			String temp = scanner.nextLine();
			if (temp.split("\\s").length != 3) {
				return;
			} else
				nodes.add(new Node(temp));
		}
		
		scanner.close();
	}

	public void countAnts() {
		int timeStamp = 1;
		int count = 0;
		for (int i = 0; i < nodes.size(); i++) {

			if (nodes.get(i).time == timeStamp)
				count++;
			else {
				System.out.println(timeStamp + " " + count);
				numAnts[(timeStamp - 1) / 100] = count;

				timeStamp += 100;
				count = 1;
			}
		}
		System.out.println(timeStamp + " " + count);
	}

	public void calcProb() {

		float count = 0;
		for (int groups = 50; groups > 0; groups--) {
			for (int i = 0; i < 101; i++) {
				if (numAnts[i] == groups)
					count++;
			}
			System.out.format("%d %5.2f%% %n", groups, count/101*100);

			count = 0;
		}
	}

	public static void main(String[] args) {
		Ants2 a2 = new Ants2();
		try {
			a2.readInput(args[0]);
		} catch (IOException e) {
			e.printStackTrace();
		}
		a2.countAnts();
		a2.calcProb();
	}

}
