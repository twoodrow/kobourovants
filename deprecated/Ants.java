package deprecated;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

@Deprecated
public class Ants {

	private class Node {

		private double xPos;
		private double yPos;
		private int time;

		public Node(String line) {
			String[] values = line.split("\\s");
			time = Integer.valueOf(values[0]);
			xPos = Double.valueOf(values[2]);
			yPos = Double.valueOf(values[3]);
		}
	}

	public Node[][] dataPoints;

	public Ants(int numAnts) {
		dataPoints = new Node[numAnts][101];

	}

	public void readInput(String fileName, int antNum) throws IOException {

		Scanner scanner = new Scanner(new FileInputStream(fileName));
		if (!scanner.hasNext()) {
			System.out.println("Input file is empty.");
			System.exit(1);
		}

		scanner.nextLine(); // first 2 lines are just 1
		scanner.nextLine();

		for (int i = 0; i < 101; i++) {
			dataPoints[antNum][i] = new Node(scanner.nextLine());
		}
		
		scanner.close();
	}

	public void printNode(Node n) {
		System.out.format("%-5d  %.4f %.4f%n", n.time, n.xPos, n.yPos);
	}

	public double distance(Node n1, Node n2) {
		return Math.sqrt(Math.pow((n1.xPos - n2.xPos), 2)
				+ Math.pow((n1.yPos - n2.yPos), 2));
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Ants a = new Ants(args.length);

		for (int i = 0; i < args.length; i++) {
			a.readInput(args[i], i);
			for (int j = 0; j < 101; j++)
				a.printNode(a.dataPoints[i][j]);
		}
	}

}
