package ant_statistics;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class PartitionCell<C extends Comparable<C>> {

	// vars
	private final C lower, upper;
	private final BoundInclusion rule;
	private ArrayList<C> elements = new ArrayList<C>();
	
	
	public PartitionCell(C lowerBound, C upperBound, BoundInclusion membershipRule)
	{
		lower = lowerBound;
		upper = upperBound;
		rule = membershipRule;
	}
	
	public boolean checkMembershipOf(C element)
	{
		boolean membership = false;
		int low = element.compareTo(lower);
		int high = element.compareTo(upper);
		
		switch(rule)
		{
		case ExcludeExclude:
			membership = (low > 0) && (high < 0);
			break;
		case ExcludeInclude:
			membership = (low > 0) && (high <= 0);
			break;
		case IncludeExclude:
			membership = (low >= 0) && (high < 0);
			break;
		case IncludeInclude:
			membership = (low >= 0) && (high <= 0);
			break;
		}
		
		return membership;
	}
	
	/**
	 * Attempts to add an element to the PartitionCell.
	 * @param item
	 * @return true if the element was added, false otherwise.
	 */
	public boolean addElement(C item)
	{
		return checkMembershipOf(item) ? elements.add(item) : false;
	}
	
	public C[] getMembers()
	{
		C[] obj = (C[]) Array.newInstance(upper.getClass(), elements.size());
		return elements.toArray(obj);
	}
	
	public C getLowerBound()
	{
		return lower;
	}

	public C getUpperBound()
	{
		return upper;
	}
}
