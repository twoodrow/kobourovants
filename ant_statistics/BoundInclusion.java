package ant_statistics;

/**
 * Rule for determining an element's membership to a PartitionCell if the element
 * is equivalent to the lower or upper bound.
 * EE - exclude elements equivalent to both lower and upper bounds.
 * EI - exclude lower, include upper.
 * IE - include lower, exclude upper.
 * II - include lower, include upper.
 * @author Travis
 *
 */
public enum BoundInclusion {
	ExcludeExclude,
	ExcludeInclude,
	IncludeExclude,
	IncludeInclude;
}
