package ant_statistics;

import java.util.Random;

public class Histogram {

	// main data member
	private int[] data;
	private double[] backingDoubles = null;
	private double[][] histBounds = null;
	
	// statistics vars
	public final int populationSize;
	public final double mean;
	// these two can be complicated
	public final double median;
	public final int modeIndex;	
	public final double mode;
	public final boolean medianIsUnique;
	public final boolean isUnimodal;
	public final boolean isOld;
	
	// random generator used to get random value
	private Random rand = new Random();
	
	/**
	 * Creates a Histogram from an naieve histogram (array of ints).
	 * @param dataPoints the int array to analyze
	 * @param calculateImmediately gathers statistics immediately rather than a later time
	 */
	public Histogram(int[] dataPoints)
	{
		isOld = true;
		// place dataPoints into 
		data = dataPoints.clone();
		
		int temp = 0;
		for(int i = 0; i < data.length; i++)
			temp += data[i];
		
		populationSize = temp;
		
		// do statistics
		mean = calculateMean();
		median = calculateMedian();
		modeIndex = calculateModeIndex();
		mode = calculateMode(modeIndex);
		isUnimodal = computeIsUnimodal();
		int mid = data.length / 2;
		medianIsUnique = (mid%2 != 1) && (data[mid] == data[mid-1]);
	}

	/**
	 * Construct a new Histogram from oldHist, a histogram with isOld==true, by
	 * adding 
	 * @param dataPoints
	 * @param binMean
	 */
	public Histogram(Histogram oldHist, double[] binMean)
	{
		isOld = false;
		
		// copy data from oldHist
		backingDoubles = new double[oldHist.data.length];
		for(int i = 0; i < oldHist.data.length; i++)
		{
			data[i] = oldHist.data[i];
			backingDoubles[i] = oldHist.data[i];
		}
		
		// load into histBounds mean
		histBounds = new double[3][];
		histBounds[2] = binMean.clone();
		
		
		populationSize = oldHist.populationSize;
		
		// do statistics
		mean = calculateMean();
		median = calculateMedian();
		modeIndex = calculateModeIndex();
		mode = calculateMode(modeIndex);
		isUnimodal = computeIsUnimodal();
		int mid = populationSize / 2;
		int sub1 = getSubjectAt(mid);
		int sub2 = getSubjectAt(mid-1);
		medianIsUnique = (mid%2 != 1) && (data[sub1] == data[sub2]);
	}
	
	/**
	 * Constructs a histogram given arbitrary, comparable types and a partition.
	 * Number of bins will be one less than the size of the partition, therefore a partition
	 * of one element will throw an error. A specimen S will belong to bin i if
	 * S > partition[i] && S < partition[i+1] evaluates to true. If S cannot be placed into
	 * any bin, the constructor will throw an error.
	 * @param specimen
	 * @param partition
	 * @throws Exception 
	 */
	public Histogram(Partition p) throws Exception
	{
		isOld = false;
		// vars
		data = new int[p.getNumberOfCells()];
		histBounds = new double[3][];
		for(int i = 0; i < 3; i++)
			histBounds[i] = new double[data.length];
		
		// build
		int popSize = 0;
		for(int i = 0; i < data.length; i++)
		{
			histBounds[0][i] = p.getCellLowerBound(i);
			histBounds[1][i] = p.getCellUpperBound(i);
			// mean
			histBounds[2][i] = (histBounds[0][i] + histBounds[1][i]) / 2;
			
			Double[] c = p.getCellElements(i);
			popSize += c.length;
			data[i] = c.length;
		} populationSize = popSize;
		
		// store doubles
		backingDoubles = new double[popSize];
		int offset = 0;
		for(int i = 0; i < data.length; i++)
		{
			Double[] c = p.getCellElements(i);
			for(int idx = 0; idx < data[i]; idx++)
				backingDoubles[idx + offset] = c[idx];

			offset += data[i];
		}

		// do statistics
		mean = calculateMean();
		median = calculateMedian();
		modeIndex = calculateModeIndex();
		mode = calculateMode(modeIndex);
		// debug
		System.out.println("modeIndex: " + modeIndex);
		System.out.println("mode: " + mode);
		isUnimodal = computeIsUnimodal();
		int mid = backingDoubles.length / 2;
		medianIsUnique = (backingDoubles.length%2 != 1) && (backingDoubles[mid] == backingDoubles[mid-1]);
	}
	
	/**
	 * Gets the subject at index from the histogram's ordered population.
	 * @param index
	 * @return
	 */
	private int getSubjectAt(int index)
	{
		// find the histogram bin which corresponds to the index
		int i = 0;
		while(index != 0)
		{
			if(index > data[i])
				index -= data[i++];
			else
				index = 0;
		}
		
		return i;
	}

	/**
	 * Calculates the mode of the data set
	 */
	private int calculateModeIndex()
	{
		int mode = Integer.MIN_VALUE;
		int index = 0;
		
		// get the mode index
		for(int i = 0; i < data.length; i++)
			if(mode < data[i])
			{
				mode = data[i];
				index = i;
			}
		
		return index;
	}
	
	private double calculateMode(int index)
	{
		if(isOld)
			return index;
		else
			return histBounds[2][index];
	}
	
	private boolean computeIsUnimodal()
	{
		int val = Integer.MIN_VALUE;
		int idx = -1;
		
		// get first mode
		for(int i = 0; i < data.length; i++)
			idx = (val < data[i])? i : idx;
		
		if(idx == -1)
			return false;
		
		// check for second mode 
		int i = 0;
		for(i=0; i < data.length; i++)
			if(data[idx] == data[i++] && i != idx)
				return true;
		
		return false;
	}
	
	/**
	 * Calculates the middle element from the histogram population.
	 */
	private double calculateMedian()
	{
		int median1 = 0, median2 = 0;
		int toSkip = populationSize / 2;
		// may need to check two elements
		boolean checkTwo = (populationSize % 2 != 0);
		
		// finds the first center element
		int i = 0;
		int leftover = 0;
		while(toSkip != 0)
		{
			// if bin has less elements than there are to skip, skip that bin entirely.
			if(toSkip > data[i])
				toSkip -= data[i++];
			else
			{
				// the element must belong to this bin
				leftover = data[i] - toSkip;
				median1 = i++;
				break;
			}
		}
		
		if(checkTwo)
		{
			// edge case, send median is not the same as median1
			if(leftover == 0)
			{
				// find second median
				while(data[i] != 0)
					i++;
				
				// i is now median2
				median2 = i;
			}
			else
			{
				median2 = median1;
			}
		}
		else
			median2 = median1;
		
		if(backingDoubles == null)
			return (median1 + median2) / 2;
		else
		{
			double m1 = histBounds[0][median1];
			double m2 = histBounds[1][median2];
			return (m1 + m2) / 2;
		}
	}
	
	private boolean computeIsMedianUnique()
	{
		int mid = populationSize / 2;
		
		// get middle values
		int sub1 = getSubjectAt(mid);
		int sub2 = getSubjectAt(mid-1);
		
		boolean ret = (mid%2 != 1) && (data[sub1] == data[sub2]);
		
		return ret;
	}
	/**
	 * Calculates the arithmetic mean of the data set
	 */
	private double calculateMean()
	{
		if(backingDoubles == null)
		{
			int runningSum = 0;
			for(int i = 0; i < data.length; i++)
				runningSum += data[i]*i;
			
			return runningSum / populationSize;
		}
		else
		{
			double runningSum = 0;
			for(int i = 0; i < backingDoubles.length; i++)
				runningSum += backingDoubles[i];
			
			return runningSum / populationSize;
		}
	}
	
	public double GetRandomValue()
	{
		int randomSubject = rand.nextInt(populationSize);
		
		if(backingDoubles == null)
			return getSubjectAt(randomSubject + 1);
		else
			return backingDoubles[randomSubject];
	}
	
	public String toBarPlotMatlabString(
			String dataName,
			String xVals,
			String xLabel,
			String yLabel,
			String title)
	{
		if(isOld)
			return null;
		
		String ret = "";
		String[] tableStrs = {"Mean", "Median", "Mode", "Population size"};
		double[] tableData = {mean, median, mode, populationSize};
		String table = MatlabCommands.subplotText(tableStrs, tableData);
		ret += MatlabCommands.matlabArray(data, dataName, " %d") + "\n";
		ret += MatlabCommands.matlabArray(histBounds[2], xVals, "%f") + "\n";		
		ret += MatlabCommands.twoBarPlot(xVals, dataName, xLabel, yLabel, title, table) + "\n";
		
		return ret;
	}
}
