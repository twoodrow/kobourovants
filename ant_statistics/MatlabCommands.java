package ant_statistics;

public final class MatlabCommands {
	private MatlabCommands(){};
	
	public static String matlabArray(double[] vals, String name, String format)
	{
		String toReturn = "";
		toReturn += name + " = [";
		
		for(int i = 0; i < vals.length-1; i++)
			toReturn += String.format(format + ", ", vals[i]);
		toReturn += String.format(format + "];", vals[vals.length-1]);
		
		return toReturn;
	}
	
	public static String matlabArray(String[] vals, String name)
	{
		String toReturn = "";
		toReturn += name + " = {";
		
		for(int i = 0; i < vals.length-1; i++)
			toReturn += "'" + vals[i] + "', ";
		toReturn += "'" + vals[vals.length-1] + "'};";
		
		return toReturn;
	}
	
	public static String matlabArray(int[] vals, String name, String format)
	{
		String toReturn = "";
		toReturn += name + " = [";
		
		for(int i = 0; i < vals.length-1; i++)
			toReturn += String.format(format + ", ", vals[i]);
		toReturn += String.format(format + "];", vals[vals.length-1]);
		
		return toReturn;
	}
	
	public static String matlabPlot(String[] xDir, String[] yDir) throws Exception
	{
		if(xDir.length != yDir.length)
			throw new Exception("xDir and yDir must have the same length.");
		
		String toReturn = "";
		toReturn += "plot(";
		for(int i = 0; i < xDir.length; i++)
				toReturn += xDir[i] + ", " + yDir[i];
		
		toReturn += ");\n";
		
		return toReturn;
	}
	
	public static String plotAxis(int x1, int x2, int y1, int y2)
	{
		String toReturn = "";
		toReturn += "axis([";
		toReturn += x1 + ",";
		toReturn += x2 + ",";
		toReturn += y1 + ",";
		toReturn += y2;
		toReturn += "]);\n";
		return toReturn;
	}
	
	public static String horizontalUitable(
			String tableName, 
			String handle,
			String[] dataNames, 
			double[] data)
	{
		String ret = "";
		ret += MatlabCommands.matlabArray(dataNames, tableName + "col") + "\n";
		ret += MatlabCommands.matlabArray(data, tableName + "data", "%f") + "\n";
		ret += tableName + " = ";
		ret += "uitable(";
		ret += "'Parent', " + handle + ",";
		ret += "'Data'," + tableName + "data,";
		ret += "'ColumnName'," + tableName + "col";// + ",";
        //'RowName',rnames,
		//'Position',[20 20 360 100]);
		ret += ")";
		return ret;
	}
	
	public static String subplotText(
			String[] dataNames, 
			double[] data)
	{
		String ret = "";
		ret += "text(0.5,0.5,'";
		for(int i = 0; i < dataNames.length-1; i++)
			ret += String.format(" " + dataNames[i] + " = %.3f,", data[i]);
		ret += String.format(" " + dataNames[dataNames.length-1] + " = %.3f',", data[dataNames.length-1]);
		
		ret += "'FontSize',14,'HorizontalAlignment','center'";
		ret += ")";
		return ret;
	}
	
	/**
	 * Returns a string of Matlab code that will plot two bar graphs, one
	 * with a logarithmic y-axis.
	 * @param xVals
	 * @param data
	 * @param labelX
	 * @param labelY
	 * @param titleStr
	 * @param uitableStr may be set to null
	 * @return
	 */
	public static String twoBarPlot(
			String xVals, 
			String data, 
			String labelX, 
			String labelY, 
			String titleStr)
	{
		String ret = "";
		
		ret += "figure\n";
		ret += "subplot(2,1,1)\n";
		ret += "bar(" + xVals + "," + data + ")\n";
		ret += xLabel(labelX) + yLabel(labelY) + title(titleStr);
		ret += "subplot(2,1,2)\n";
		ret += "bar(" + xVals + "," + data + ")\n";
		ret += xLabel(labelX) + yLabel(labelY) + title(titleStr + " (logarithmic y-axis)");
		ret += "set(gca,'YScale','log');\n";
		
		return ret;
	}
	
	/**
	 * Returns a string of Matlab code that will plot two bar graphs, one
	 * with a logarithmic y-axis, and a privided uitable with some data.
	 * @param xVals
	 * @param data
	 * @param labelX
	 * @param labelY
	 * @param titleStr
	 * @param uitableStr may be set to null
	 * @return
	 */
	public static String twoBarPlot(
			String xVals, 
			String data, 
			String labelX, 
			String labelY, 
			String titleStr,
			String textStr)
	{
		String ret = "";
		
		ret += "figure;\n";
		ret += "subplot(3,1,1);\n";
		ret += "bar(" + xVals + "," + data + ");\n";
		ret += xLabel(labelX) + yLabel(labelY) + title(titleStr);
		ret += "subplot(3,1,2);\n";
		ret += "bar(" + xVals + "," + data + ");\n";
		ret += xLabel(labelX) + yLabel(labelY) + title(titleStr + " (logarithmic y-axis);");
		ret += "set(gca,'YScale','log');\n";
		ret += "subplot(3,1,3);\n";
		ret += textStr + ";";
		
		return ret;
	}
	
	public static String xLabel(String str)
	{
		return String.format("xlabel('" + str + "')\n");
	}
	public static String yLabel(String str)
	{
		return String.format("ylabel('" + str + "')\n");
	}
	public static String title(String str)
	{
		return String.format("title('" + str + "')\n");
	}
}
