package ant_statistics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import ant_base.AntGraph;
import ant_base.AntNode;
import ant_io.GraphReader;
import ant_mysql.AntState;

public final class Statistics {
	
	// Statistics should never be instantiated
	private Statistics(){};

	public static int[] ExtractNodeProbabilities(AntGraph graph) {
		TreeMap<Integer, ArrayList<AntNode>> map = (TreeMap<Integer, ArrayList<AntNode>>) graph
				.getNodesByTimestamp();
		Set<Integer> keys = map.keySet();
		int[] timestamps = new int[keys.size()];
		int position = 0;

		for (Integer i : keys) {
			ArrayList<AntNode> list = map.get(i);
			timestamps[position] = list.size();
			position++;
		}

		int[] result = new int[graph.getNumAnts() + 1];

		for (int i = 0; i < timestamps.length; i++) {
			result[timestamps[i]]++;
		}
		return result;
	}

	public static int[] FindNodeDegrees(AntGraph graph) {
		TreeMap<Integer, ArrayList<AntNode>> map = (TreeMap<Integer, ArrayList<AntNode>>) graph
				.getNodesByInDegree();
		Set<Integer> keys = map.keySet();
		int[] result = new int[keys.size()];
		int position = 0;

		for (Integer i : keys) {
			ArrayList<AntNode> list = map.get(i);
			result[position] = list.size();
			position++;
		}

		return result;
	}

	// old methods
	@Deprecated
	public static double[] FindDistanceSim(int size)
			throws FileNotFoundException {
		double[] result = new double[size];
		double x1, x2, y1, y2;
		x1 = x2 = y1 = y2 = -2;
		double dist = 0;

		String filename;
		// for (int i = 0; i < size; i++) {
		// if (i < 9)
		// filename = "groundtruth1\\GT_ant" + "0" + (i + 1) + ".txt";
		// else
		// filename = "groundtruth1\\GT_ant" + (i + 1) + ".txt";
		// }

		for (int i = 0; i < size; i++) {
			filename = "simAnt" + (i + 1) + ".txt";
			FileInputStream input = new FileInputStream(filename);
			Scanner scanner = new Scanner(input);
			scanner.nextLine();
			scanner.nextLine();
			dist = 0;
			x1 = x2 = y1 = y2 = -2;
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] data = line.split("\\s");
				x1 = x2;
				y1 = y2;
				x2 = Double.valueOf(data[1]);
				y2 = Double.valueOf(data[2]);
				if (x1 != -2) {
					dist = distance(x1, y1, x2, y2);
					result[i] += dist;
				}
			}
			
			scanner.close();
		}

		return result;
	}
	@Deprecated
	public static double[] FindDistanceGT1(int size)
			throws FileNotFoundException {
		double[] result = new double[size];
		double x1, x2, y1, y2;
		x1 = x2 = y1 = y2 = -2;
		double dist = 0;

		String filename;
		
		for (int i = 0; i < size; i++) {
			if (i < 9)
				filename = "groundtruth1\\GT_ant" + "0" + (i + 1) + ".txt";
			else
				filename = "groundtruth1\\GT_ant" + (i + 1) + ".txt";
			
			FileInputStream input = new FileInputStream(filename);
			Scanner scanner = new Scanner(input);
			scanner.nextLine();
			scanner.nextLine();
			dist = 0;
			x1 = x2 = y1 = y2 = -2;
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] data = line.split("\\s");
				x1 = x2;
				y1 = y2;
				x2 = Double.valueOf(data[2]);
				y2 = Double.valueOf(data[3]);
				if (x1 != -2) {
					dist = distance(x1, y1, x2, y2);
					result[i] += dist;
				}
			}
			
			scanner.close();
		}

		return result;
	}

	/**
	 * Calculates the euclidean distance between points 1 and 2.
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	public static double distance(double x1, double y1, double x2, double y2) {
		return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
	}
	
	/**
	 * Returns the smallest angle between lines 12 and 23. 
	 * @param x1 
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param x3
	 * @param y3
	 * @return
	 */
	public static double calcTrajectoryAngle(double x1, double y1, double x2, double y2, double x3, double y3)
	{
		double direction1 = Math.toDegrees(Math.atan2(y2-y1, x2-x1));
		double direction2 = Math.toDegrees(Math.atan2(y3-y2, x3-x2));
		
		double difference = Math.abs(direction2 - direction1);
		
		// max can only be 180
		if(difference > 180.0)
			difference = 360.0 - difference;
		
		return difference;
	}
	
	
	/**
	 * Returns all delta turn amounts for specific antId within colony.
	 * @param colony
	 * @param antId
	 * @return null, if antId is not in colony's keyset.
	 */
	public static double[] getAntDeltaDegrees(TreeMap<Integer, AntState[]> colony, int antId)
	{
		if(!colony.containsKey(antId))
			return null;
		
		AntState[] ant = colony.get(antId);
		double[] deltas = new double[ant.length-2];
		for(int i = 0; i < deltas.length-2; i++)
		{
			AntState t1 = ant[i];
			AntState t2 = ant[i+1];
			AntState t3 = ant[i+2];				
			deltas[i] += calcTrajectoryAngle(t1.x, t1.y,t2.x, t2.y,t3.x, t3.y);
		}
		
		return deltas;
	}
	
	/**
	 * Returns an array containing all delta distances for a specific ant within a colony data set.
	 * @param colony
	 * @param antId
	 * @return
	 */
	public static double[] getAntDeltaDistances(TreeMap<Integer, AntState[]> colony, int antId)
	{
		if(!colony.containsKey(antId))
			return null;
		
		AntState[] ant = colony.get(antId);
		double[] deltas = new double[ant.length-1];
		for(int i = 0; i < deltas.length; i++)
		{
			AntState t1 = ant[i], t2 = ant[i+1];
			deltas[i] = distance(t1.x, t1.y, t2.x, t2.y);
		}
		
		return deltas;
	}
	
	/**
	 * Returns the delta distances for every ant in the colony.
	 * @param colony
	 * @return
	 */
	public static double[] getColonyDeltaDistances(TreeMap<Integer, AntState[]> colony)
	{
		double[][] allDeltas = new double[colony.size()][];
		int i = 0;
		for(int key : colony.keySet())
			allDeltas[i++] = getAntDeltaDistances(colony, key);
		
		return collapse2DArray(allDeltas);
	}
	
	/**
	 * Returns the delta angles for every ant in the colony.
	 * @param colony
	 * @return
	 */
	public static double[] getColonyDeltaDegrees(TreeMap<Integer, AntState[]> colony)
	{
		double[][] allDeltas = new double[colony.size()][];
		int i = 0;
		for(int key : colony.keySet())
			allDeltas[i++] = getAntDeltaDegrees(colony, key);
		
		return collapse2DArray(allDeltas);
	}
	
	/**
	 * Returns the path length of the ant with the corresponding antId within colony.
	 * @param colony
	 * @param antId
	 * @return
	 */
	public static double getAntPathDistance(TreeMap<Integer, AntState[]> colony, int antId)
	{
		double total = 0;
		double[] deltas = getAntDeltaDistances(colony, antId);
		
		for(double d : deltas)
			total += d;
		
		return total;
	}
	
	/**
	 * Returns the total amount of degrees the ant has turned while traversing its path.
	 * @param colony
	 * @param antId
	 * @return
	 */
	public static double getAntTotalDegrees(TreeMap<Integer, AntState[]> colony, int antId)
	{
		double total = 0;
		double[] deltas = getAntDeltaDegrees(colony, antId);
		
		for(double d : deltas)
			total += d;
		
		return total;
	}
	
	/**
	 * Returns the path distance for every ant in the colony.
	 * @param colony
	 * @return
	 */
	public static double[] getColonyPathDistances(TreeMap<Integer, AntState[]> colony)
	{
		int size = colony.size();
		double[] degreeTotals = new double[size];
		
		for(int id : colony.keySet())
			degreeTotals[id] = getAntPathDistance(colony, id);
		
		return degreeTotals;
	}
	
	/**
	 * Returns the total degrees turned for every ant in the colony.
	 * @param colony
	 * @return
	 */
	public static double[] getColonyTotalDegrees(TreeMap<Integer, AntState[]> colony)
	{
		int size = colony.size();
		double[] degreeTotals = new double[size];
		
		for(int id : colony.keySet())
			degreeTotals[id] = getAntTotalDegrees(colony, id);
		
		return degreeTotals;
	}
	
	/**
	 * Returns a count of elements in angleDeltas which satisfy: element > 180-sharpTurnThreshold.
	 * @param angleDeltas
	 * @return
	 */
	public static int getSharpTurnCount(double[] angleDeltas, double sharpTurnThreshold)
	{
		int count = 0;
		double thresh = 180 - sharpTurnThreshold;
		
		for(double theta : angleDeltas)
			count += (theta > thresh) ? 1 : 0;
		
		return count;
	}
	
	/**
	 * Returns the number of deltaAngles that meet the sharp turn criteria from 0 to 180 degress
	 * and split into numIntervals.
	 * @param deltaAngles
	 * @param numIntervals
	 * @return
	 */
	public static int[] getSharpTurnsByDegree(double[] deltaAngles, int numIntervals)
	{
		// data
		int[] counts = new int[numIntervals];
		double[] intervals = getSharpTurnIntervals(numIntervals);
		
		// get counts
		for(int i = 0; i < numIntervals; i++)
			counts[i] = getSharpTurnCount(deltaAngles, intervals[i]);
		
		return counts;
	}
	
	/**
	 * Gets the intervals associated with the Histogram for counts of
	 * sharp turns by degree.
	 * @param numIntervals
	 * @return
	 */
	private static double[] getSharpTurnIntervals(int numIntervals)
	{
		double[] intervals = new double[numIntervals];
		
		// build intervals
		for(int i = 0; i < numIntervals; i++)
			intervals[i] = ((double)(i+1) * 180.0) / (double)numIntervals;
		
		return intervals;
	}
	
	/**
	 * Takes all elements from a 2D double array and places them into a 1D double array.
	 * @param data
	 * @return
	 */
 	public static double[] collapse2DArray(double[][] data)
	{
		// first get total number of elements
		int retSize = 0;
		for(double[] arr : data)
			retSize += arr.length;
		
		// then copy data over
		int i = data.length - 1;
		double[] ret = new double[retSize];
		while(--i > -1)
		{
			retSize -= data[i].length;
			System.arraycopy(data[i], 0, ret, retSize, data[i].length);
		}
		
		return ret;
	}
 	
 	/**
 	 * Processes the colony and returns strings to generate Matlab plots of individual ant's total angular
 	 * change, path length, and a colony's delta angle distribution, delta distances, and distribution of
 	 * sharp turns. Data will be plotted in histograms consisting of the number of intervals specified by
 	 * numIntervals.
 	 * @param colony
 	 * @param numIntervals
 	 * @return
 	 * @throws Exception 
 	 */
 	public static String extractColonyMetrics(
 			TreeMap<Integer, AntState[]> colony,
 			int numIntervals,
 			String colonyStr) throws Exception
 	{
 		String ret = "";
 		
 		// data
 		double[] totalAngle, pathLength, deltaAngles, deltaDist;
 		int[] sharpTurns;
 		Histogram hTA, hPL, hST, hDA, hDD;
 		
 		// compute values
 		totalAngle = getColonyTotalDegrees(colony);
 		pathLength = getColonyPathDistances(colony);
 		deltaAngles = getColonyDeltaDegrees(colony);
 		deltaDist = getColonyDeltaDistances(colony);
 		sharpTurns = getSharpTurnsByDegree(deltaAngles, numIntervals);
 		
 		// create histograms
 		hTA = new Histogram(new Partition(totalAngle, numIntervals));
 		hPL = new Histogram(new Partition(pathLength, numIntervals));
 		hDA = new Histogram(new Partition(deltaAngles, numIntervals));
 		hDD = new Histogram(new Partition(deltaDist, numIntervals));
 		hST = new Histogram(new Histogram(sharpTurns), getSharpTurnIntervals(numIntervals));
 		
 		// append strings
 		String ac = "Ant count";
 		ret += hTA.toBarPlotMatlabString("TA", "X_1", "Total Ant Angle", ac, colonyStr);
 		ret += hPL.toBarPlotMatlabString("PL", "X_2", "Ant Path Length", ac, colonyStr);
 		ret += hST.toBarPlotMatlabString("ST", "X_3", "Sharp turns by angle", ac, colonyStr);
 		ret += hDA.toBarPlotMatlabString("DA", "X_4", "Ant timestamp delta angles", ac, colonyStr);
 		ret += hDD.toBarPlotMatlabString("DD", "X_5", "Ant timestamp delta distance", ac, colonyStr);
 		
 		return ret;
 	}
 	
 	public static int[][] getColonyCliquePersistence(AntGraph g)
 	{
 		int[][] ret;
 		// vars
 		Map<Integer, ArrayList<AntNode>> nodes = g.getNodesByTimestamp();
 		int maxSize = nodes.size()+1;
 		int numAnts = g.getValidAntIds().length;
 		
 		ret = new int[numAnts+1][];
 		for(int i = 0; i < ret.length; i++)
 			ret[i] = new int[maxSize];
 		
 		String[] persist = new String[2*maxSize];
 		int[] persistCount = new int[persist.length];
 		
 		for(int time : nodes.keySet())
 		{
 			String[] newCliques = new String[maxSize];
 			
 			// get all cliques at this time
 			ArrayList<AntNode> list = nodes.get(time);
 			int i = 0;
 			for(AntNode node : list)
 			{
 				int[] clique = g.getAntIdsAtNode(node);
 				Arrays.sort(clique);
 	 			String cliqueStr = convertIntArrayToString(clique);
 	 			newCliques[i++] = cliqueStr;
 			}
 			
 			// check cliques added
 			for(String clique : newCliques)
 			{
 				if(clique == null)
 					continue;
 				
 				int idx = arrayHasString(persist, clique);
 				if(idx != -1)
 					persistCount[idx]++;
 				else
 				{
 					idx = getFreeIndex(persist);
 					persist[idx] = clique;
 					persistCount[idx] = 0;
 				}
 			}
 			
 			// remove non-present cliques
 			for(String clique : persist)
 			{
 				if(clique == null)
 					continue;
 				
 				int idx = arrayHasString(newCliques, clique);
 				if(idx == -1)
 				{
 					int persistIdx = arrayHasString(persist, clique);
 					int cliqueSize = getIntsInDelimitedString(clique);
 					int count = persistCount[persistIdx];
 					persist[persistIdx] = null;
 					persistCount[persistIdx] = 0;
 					ret[cliqueSize][count]++;
 				}
 			}
 		}
 		
		// at the end, remove from ret
		for(String clique : persist)
		{
			if(clique == null)
				continue;
			
			int persistIdx = arrayHasString(persist, clique);
			int cliqueSize = getIntsInDelimitedString(clique);
			int count = persistCount[persistIdx];
			persist[persistIdx] = null;
			persistCount[persistIdx] = 0;
			ret[cliqueSize][count]++;
		}
 		
 		return ret;
 	}
 	
 	private static String convertIntArrayToString(int[] vals)
 	{
 		String ret = "";
 		
 		for(int val : vals)
 			ret += String.format("%d*", val);
 		
 		return ret;
 	}
 	
 	private static int getIntsInDelimitedString(String str)
 	{
 		char[] data = str.toCharArray();
 		int count = 0;
 		
 		for(int i = 0; i < data.length; i++)
 			if(data[i] == '*')
 				count++;
 		
 		return count;
 	}
 	
 	private static int arrayHasString(String[] all, String check)
 	{
 		for(int i = 0; i < all.length; i++)
 		{
 			String str = all[i];
 			if(str == null)
 				continue;
 			
 			if(str.equals(check))
 				return i;
 		}
 		return -1;
 	}
 	
 	private static int getFreeIndex(String[] all)
 	{
 		for(int i = 0; i < all.length; i++)
 			if(all[i] == null)
 				return i;
 		
 		return -1;
 	}

}
