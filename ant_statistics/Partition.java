package ant_statistics;


public class Partition {

	// member vars
	private PartitionCell<Double>[] data;
	private final boolean uniformBinSize;
	private final Double setMax, setMin, diff;
	
	
	/**
	 * Creates a partition restricted to Double only. Attempting to use other
	 * types will throw an exception.
	 * @param elements
	 * @param min
	 * @param max
	 * @param numCells
	 * @throws Exception 
	 */
	public Partition(double[] elements, int numCells)
	{		
		data = (PartitionCell<Double>[])new PartitionCell[numCells];
		uniformBinSize = true;
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		// find largest and smallest elements
		for(int i = 0; i < elements.length; i++)
		{
			min = (elements[i] < min)?  elements[i] : min;
			max = (elements[i] > max)? elements[i] : max;
		}
		
		setMax = new Double((int)(max+1));
		setMin = new Double((int)min);
		
		// debug
		System.out.println("min:" + min);
		System.out.println("setmin:" + setMin);
		System.out.println("max:" + max);
		System.out.println("setmax:" + setMax);
		
		// create cells
		diff = (setMax - setMin) / numCells;
		System.out.println("diff:" + diff);
		for(int i = 0; i < numCells-1; i++) 
		{
			data[i] = new PartitionCell<Double>(setMin + (i*diff), setMin + ((i+1)*diff), BoundInclusion.IncludeExclude);
		} data[numCells-1] = new PartitionCell<Double>(setMax-diff, setMax, BoundInclusion.IncludeInclude);
		
		// insert data
		for(int i = 0; i < elements.length; i++)
		{
			int index = (int)((elements[i] - setMin) / diff);
			data[index].addElement(elements[i]);
		}
	}
	
	public boolean addElement(Double el)
	{
		if(uniformBinSize)
		{
			int index = (int)((el - setMin) / diff);
			return data[index].addElement(el);
		}
		else
		{
			for(PartitionCell<Double> cell : data)
				if(cell.addElement(el))
					return true;
			
			return false;
		}
	}
	
	public double getCellLowerBound(int index)
	{
		return (double)data[index].getLowerBound();
	}
	
	public double getCellUpperBound(int index)
	{
		return (double)data[index].getUpperBound();
	}
	
	public int getNumberOfCells()
	{
		return data.length;
	}
	
	public Double[] getCellElements(int index)
	{
		return (Double[])data[index].getMembers();
	}
}