package ant_trajectories;

import java.util.ArrayList;
import java.util.TreeMap;

import ant_base.AntEdge;
import ant_base.AntGraph;
import ant_base.AntNode;
import ant_io.GraphReader;

public abstract class GroundTruth {

	private GroundTruth() {
	}

	public static TreeMap<Integer, ArrayList<AntNode>> create(String fileName) {
		TreeMap<Integer, ArrayList<AntNode>> trajectories = new TreeMap<Integer, ArrayList<AntNode>>();
		AntGraph graph;
		graph = GraphReader.ReadBigGraph(fileName);
		ArrayList<AntNode> nodes = graph.getBaseNodes();
		AntNode current, start;
		ArrayList<AntNode> temp = new ArrayList<AntNode>();
		TreeMap<Integer, ArrayList<AntEdge>> edges = graph
				.getEdgesByIncidentFromNodeId();
		ArrayList<AntEdge> tempEdge;

		for (int i = 0; i < 50; i++) {
			start = nodes.get(i);
			tempEdge = edges.get(i);
			temp.add(start);

			for (int j = 0; j < 98; j++) {
				current = nodes.get(tempEdge.remove(0).GetIncidentToNodeId());
				temp.add(current);
				tempEdge = edges.get(current.nodeId);
			}

			current = nodes.get(tempEdge.remove(0).GetIncidentToNodeId());
			temp.add(current);

			trajectories.put(i, temp);
			temp = new ArrayList<AntNode>();
		}
		return trajectories;
	}

	@SuppressWarnings("unchecked")
	public static TreeMap<Integer, ArrayList<AntNode>> create(AntGraph graph) {
		TreeMap<Integer, ArrayList<AntNode>> trajectories = new TreeMap<Integer, ArrayList<AntNode>>();

		ArrayList<AntNode> nodes = (ArrayList<AntNode>) graph.getBaseNodes()
				.clone();
		TreeMap<Integer, ArrayList<AntEdge>> edges = (TreeMap<Integer, ArrayList<AntEdge>>) graph
				.getEdgesByIncidentFromNodeId().clone();
		ArrayList<AntNode> temp = new ArrayList<AntNode>();

		AntNode current, start;
		ArrayList<AntEdge> tempEdge;

		for (int i = 0; i < 50; i++) {
			// starting node for this id
			start = nodes.get(i);
			tempEdge = (ArrayList<AntEdge>) edges.get(i).clone();
			temp.add(start);

			for (int j = 0; j < 49; j++) {
				current = nodes.get(tempEdge.remove(0).GetIncidentToNodeId());
				temp.add(current);
				tempEdge = edges.get(current.nodeId);
			}

			current = nodes.get(tempEdge.remove(0).GetIncidentToNodeId());
			temp.add(current);

			trajectories.put(i, temp);
			temp = new ArrayList<AntNode>();
		}

		return trajectories;
	}

}
