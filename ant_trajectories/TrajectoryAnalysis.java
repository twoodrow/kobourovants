package ant_trajectories;

import java.util.TreeMap;

public class TrajectoryAnalysis {

	// main vars
	public final int antId;
	public final int videoId;
	// first dimension is timestep
	// second is antId sorted by distance
	public final int[][] nearAntIds;
	// first dimension is antId-1 (no antID 0)	
	// second dimension is timestep
	public final TreeMap<Integer, double[]> nearAntDistances;
	public final TreeMap<Integer, double[]> nearAntClickThetas;
	
	/**
	 * Creates from the necessary data. Maintains references to the collection arguments.
	 * @param ants
	 * @param distances
	 * @param vidId
	 * @param antsId
	 */
	public TrajectoryAnalysis(int[][] ants,
			TreeMap<Integer, double[]> distances,
			TreeMap<Integer, double[]> clickAngles,
			int vidId, 
			int antsId)
	{
		nearAntIds = ants;
		nearAntDistances = distances;
		nearAntClickThetas = clickAngles;
		videoId = vidId;
		antId = antsId;
	}
	
	public String getClosestAnts(int closestCount, int timeStamp)
	{
		String ret = String.format("Ants closest to %d with videoId %d at timestamp %d\n", antId, videoId, timeStamp);
		String vals = "";
		
		if(timeStamp == 100)
			vals += ".";
		int[] relevant = nearAntIds[timeStamp];
		
		for(int i = 0; i < closestCount; i++)
		{
			boolean valid = (relevant[i] != -1);
			if(valid)
			{
				vals += String.format("%d: id:%d, dist:%f, theta:%f\n", 
					i+1, relevant[i], 
					nearAntDistances.get(relevant[i])[timeStamp],
					nearAntClickThetas.get(relevant[i])[timeStamp]);
			}
		}
		
		if(vals.compareTo("") == 0)
			vals += "No valid entries";
		
		return ret + vals;
	}
	
	
}
