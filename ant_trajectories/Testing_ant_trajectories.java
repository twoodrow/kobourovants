package ant_trajectories;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.TreeMap;

import org.junit.Test;

import ant_base.AntGraph;
import ant_io.GraphReader;
import ant_io.ParseGroundTruth;
import ant_mysql.AntState;

public class Testing_ant_trajectories {
	
	// vars
	static String pcDir = "E:\\bitbucket\\kobourovAntsData\\ant_paths\\";
	static String laptopDir = "C:\\Dropbox\\Team3-Ants\\data\\";
	static String colony2PathsDir = "newuserdata\\2\\";
	static String colony1PathsDir = "newuserdata\\1\\";
	static String gtColonyGraphDir = "C:\\Dropbox\\Team3-Ants\\data\\graphfiles\\";
	
	
	@Test
	public void UserTrajectory() throws Exception
	{
		UT_constructor();
	}
	
	// User Trajectory
	@Test
	public void UT_constructor() throws Exception
	{
		String testDir = "GT_ant01.txt";
		UserTrajectory ut1 = new UserTrajectory(testDir);
		UserTrajectory ut2 = new UserTrajectory(ut1.m_videoId, ut1.m_antId, ut1.m_states);
		assertTrue(ut1.m_antId == ut2.m_antId);
	}
	
	@Test
	public void UT_compareGroundTruth() throws Exception
	{
		// run specific
		int closest = 2;
		
		String pathStr = laptopDir + colony2PathsDir + "6-2013-12-06-14-46-22.txt";
		String gtStr = gtColonyGraphDir + "gt2.txt";
		TreeMap<Integer, AntState[]> colony6 = ParseGroundTruth.getColony06();
		AntGraph g = GraphReader.ReadBigGraph(gtStr);
		
		UserTrajectory user = new UserTrajectory(pathStr);
		
		TrajectoryAnalysis gtAnalysis = user.compareGroundTruth(colony6);
		TrajectoryAnalysis clusterAnalysis = user.compareGraph(g);
		
		for(int i = 0; i < user.m_states.length; i++)
		{
			System.out.println(gtAnalysis.getClosestAnts(closest, i));
			System.out.println(clusterAnalysis.getClosestAnts(closest, i));
		}
	}
	
	
}
