package ant_trajectories;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import org.junit.*;

import ant_base.AntGraph;
import ant_base.AntNode;
import ant_mysql.AntState;
import ant_statistics.Statistics;

public class UserTrajectory {
	
	// main vars
	public final int m_videoId;
	public final int m_antId;
	public final AntState[] m_states;
	
	/**
	 * Creates from user trajectory in it's file format.
	 * @param filename
	 * @throws Exception 
	 */
	public UserTrajectory(String filename) throws Exception
	{
		// vars
		ArrayList<AntState> list = new ArrayList<AntState>();
		Scanner reader = new Scanner(new FileInputStream(filename));
		int vId = Integer.MIN_VALUE, aId = Integer.MIN_VALUE;
		
		// read args
		if(reader.hasNextInt())
			vId = reader.nextInt();
		if(reader.hasNextInt())
			aId = reader.nextInt();
		else
		{
			reader.close();
			throw new Exception("The format required is int\nint\ndata\n.\n.\n.");
		}
		
		while(reader.hasNextLine())
		{
			String line = reader.nextLine();
			if(!line.equals(""))
			{
				String[] next = line.split("\\s");
				int t = Integer.parseInt(next[0]);
				float x = Float.parseFloat(next[1]);
				float y = Float.parseFloat(next[2]);
				list.add(new AntState(aId, t, x, y, -1));
			}
		}
		
		
		// copy to state array
		m_videoId = vId;
		m_antId = aId;
		m_states = new AntState[list.size()];		
		for(int i = 0; i < m_states.length; i++)
			m_states[i] = list.get(i);
		
		// close stream
		reader.close();
	}
	
	/**
	 * Creates from the essential data components.
	 * @param videoId
	 * @param antId
	 * @param states
	 * @throws Exception 
	 */
	public UserTrajectory(int videoId, int antId, AntState[] states) throws Exception
	{
		// check args
		if(states == null)
			throw new Exception("states cannot be null.");
		
		m_videoId = videoId;
		m_antId = antId;
		m_states = states;
	}

	public TrajectoryAnalysis compareGraph(AntGraph g)
	{
		// vars
		TrajectoryAnalysis ret = null;		
		Map<Integer, ArrayList<AntNode>> nodes = g.getNodesByTimestamp();
		int[] ants = g.getValidAntIds();
		int numAnts = ants.length;
		int numTimeStamps = m_states.length;
		int[][] gtAntProximity = new int[numTimeStamps][];
		TreeMap<Integer, double[]> gtAntDeltaDist = new TreeMap<Integer, double[]>();
		TreeMap<Integer, double[]> gtAntClickAngle = new TreeMap<Integer, double[]>();
		for(int i = 0; i < numTimeStamps; i++)
		{
			gtAntProximity[i] = new int[numAnts];
			Arrays.fill(gtAntProximity[i], -1);
		}
		for(int id : ants)
		{
			gtAntDeltaDist.put(id, new double[numTimeStamps]);
			gtAntClickAngle.put(id, new double[numTimeStamps]);
		}
		
		// calculate distances
		int t = 0;
		for(int time : nodes.keySet())
		{
			AntState cur = m_states[t];
			for(AntNode node : nodes.get(time))
			{
				// get x,y of node
				double dx = cur.x - node.xPos;
				double dy = cur.y - node.yPos;
				double dD = Statistics.distance(dx, dy, 0,0);
				double theta = Math.toDegrees(Math.atan2(dy, dx));
				
				int[] antIds = g.getAntIdsAtNode(node);				
				for(int id : antIds)
				{
					gtAntDeltaDist.get(id)[t] = dD;
					gtAntClickAngle.get(id)[t] = theta;
				}
			}
			
			t++;
		}
		
		// sort by delta distances
		sortAntIdsByDeltaDistances(gtAntProximity, gtAntDeltaDist);
		
		return new TrajectoryAnalysis(gtAntProximity, gtAntDeltaDist, 
				gtAntClickAngle, m_videoId, m_antId);
	}
	
	public TrajectoryAnalysis compareGroundTruth(TreeMap<Integer, AntState[]> groundTruthData)
	{
		// vars
		TrajectoryAnalysis ret = null;
		int numAnts = groundTruthData.size();
		int numTimeStamps = m_states.length;
		int[][] gtAntProximity = new int[numTimeStamps][];
		TreeMap<Integer, double[]> gtAntDeltaDist = new TreeMap<Integer, double[]>();
		TreeMap<Integer, double[]> gtAntClickAngle = new TreeMap<Integer, double[]>();
		for(int i = 0; i < numTimeStamps; i++)
		{
			gtAntProximity[i] = new int[numAnts];
			Arrays.fill(gtAntProximity[i], -1);
		}
		for(int id : groundTruthData.keySet())
		{
			gtAntDeltaDist.put(id, new double[numTimeStamps]);
			gtAntClickAngle.put(id, new double[numTimeStamps]);
		}
		
		// calculate distances and angles
		for(int id : groundTruthData.keySet())
		{
			AntState[] idStates = groundTruthData.get(id);
			for(int t = 0; t < numTimeStamps; t++)
			{
				AntState cur = m_states[t];
				double dx = cur.x - idStates[t].x;
				double dy = cur.y - idStates[t].y;
				double dD = Statistics.distance(dx, dy, 0,0);
				double theta = Math.toDegrees(Math.atan2(dy, dx));
				gtAntDeltaDist.get(id)[t] = dD;
				gtAntClickAngle.get(id)[t] = theta;
			}
		}
		
		// sort
		sortAntIdsByDeltaDistances(gtAntProximity, gtAntDeltaDist);		

		return new TrajectoryAnalysis(gtAntProximity, gtAntDeltaDist, 
				gtAntClickAngle, m_videoId, m_antId);
	}

	/**
	 * Uses selection sorts antIds by their corresponding values in antDeltas.
	 * @param antIds
	 * @param antDeltas
	 */
	private void sortAntIdsByDeltaDistances(int[][] antIds, TreeMap<Integer, double[]> antDeltas)
	{
		int numTimeStamps = antDeltas.firstEntry().getValue().length;
		int numAnts = antDeltas.size();
		TreeMap<Integer, double[]> d = antDeltas; // for brevity while typing
		
		for(int t = 0; t < numTimeStamps; t++)
		{
			int[] curT = antIds[t];
			
			// selection sort
			double prev = (-1)*Double.MAX_VALUE;
			int[] picked = new int[antDeltas.lastKey() + 2];
			for(int i = 0; i < numAnts; i++)
			{
				double curLeast = Double.MAX_VALUE;
				int idCurLeast = -1;
				for(int id : antDeltas.keySet())
				{
					double val = d.get(id)[t];
					
					boolean biggerThanLast = (prev <= val);
					boolean smallerThanCurrentLeast = (val < curLeast);
					boolean valid = (0 <= val);
					boolean notPicked = (0 == picked[id + 1]);
					
					if(biggerThanLast && smallerThanCurrentLeast && valid && notPicked)
					{
						curLeast = val;
						idCurLeast = id;
					}
				}
				
				picked[idCurLeast + 1] = 1;
				prev = curLeast;
				curT[i] = idCurLeast;
			}
			
			// check for same delta values as antId
			if(t == 7)
				t = 7;
			int idx = 0;
			// find m_antId's rank in curT and get its value
			while( (curT[idx] != m_antId) && (idx < curT.length) )
				idx++;
			double idVal = d.get(curT[idx])[t];
			int start = -1, end = -1;
			// iterate through all other ids to check for duplicate distances equal to m_antId
			for(int i = 0; i < curT.length; i++)
			{
				// check valid
				int id = curT[i];
				if(id == -1)
					continue;
				
				// check for same graph node
				double val = d.get(id)[t];
				if(val == idVal)
				{
					// hits once
					if(start == -1)
						start = i;
					
					// hits until no more vals
					if(start != -1)
						end = i;
				}
			}
			
			if(start == end)
				continue;
			else
			{
				// swap ids
				curT[idx] = curT[start];
				curT[start] = m_antId;
			}
			
		}
	}
}
