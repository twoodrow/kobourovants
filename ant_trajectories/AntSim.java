package ant_trajectories;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.TreeMap;
import ant_mysql.AntState;
import java.util.Random;
import ant_base.SimAnt;

public class AntSim {

	private class Point {
		private double x, y;

		public Point(double xp, double yp) {
			x = xp;
			y = yp;
		}
	}

	private ArrayList<SimAnt> ants = new ArrayList<SimAnt>();
	private int timestamps;
	private int time;
	private int numAnts;

	public AntSim(int numbAnts, int timeStamps) {
		numAnts = numbAnts;
		timestamps = timeStamps;
		time = 1;
	}

	public void runSim() throws IOException {

		String fileName;
		String fileName2;
		int counter = 1;
		Random rand = new Random();
		double spec;

		for (int i = 0; i < numAnts; i++) {
			spec = (i + 1);
			spec = spec / (numAnts + 1);
			ants.add(new SimAnt(spec, i));
		}

		for (SimAnt a : ants) {
			fileName = "simAnt" + counter + ".txt";
			fileName2 = "simAntD" + counter + ".txt";
			time = 1;

			Formatter writer = new Formatter(fileName);
			Formatter writer2 = new Formatter(fileName2);
			writer.format("1\n%d\n", counter);
			writer2.format("1\n%d\n", counter);

			writer.format("%d %.4f %.4f\n", time, a.getXPos(), a.getYPos());
			writer2.format("%d %.4f %.4f %.4f\n", time, a.getXPos(),
					a.getYPos(), a.getDirection());

			for (int i = 0; i < (timestamps * 10); i++) {
				time += 10;
				a.turn();
				if (rand.nextInt(100) < 12)
					a.uTurn();
				findCollision(a);
				if (!a.getCollision() && rand.nextInt(100) > 23) {
					a.move();
				}
				if (time % 100 == 1) {
					writer.format("%d %.4f %.4f\n", time, a.getXPos(),
							a.getYPos());
					writer2.format("%d %.4f %.4f %.4f\n", time, a.getXPos(),
							a.getYPos(), a.getDirection());
				}
			}
			counter++;
			writer.close();
			writer2.close();
		}
	}
	
	public TreeMap<Integer, AntState[]> runSimOutputStrings()
	{
		double spec;
		Random rand = new Random();
		TreeMap<Integer, AntState[]> states = new TreeMap<Integer, AntState[]>();
		for(int i = 1; i <= numAnts; i++)
			states.put(i, new AntState[timestamps + 1]);

		for (int i = 0; i < numAnts; i++) {
			spec = (i + 1);
			spec = spec / (numAnts + 1);
			ants.add(new SimAnt(spec, i));
		}
		
		int id = 1;		
		// first timestamp
		for(SimAnt a : ants)
			states.get(id++)[0] = new AntState(id, 1, (float)a.getXPos(), (float)a.getYPos(), (float)a.getDirection());

		// remaining timestamps
		int idx = 1;
		for (int i = 0; i < (timestamps * 10)+1; i++) {
			time += 10;
			id = 1;			
			
			for(SimAnt a : ants)
			{
				a.turn();
				if (rand.nextInt(100) < 5)
					a.uTurn();
				findCollision(a);
				if (!a.getCollision() && rand.nextInt(100) > 23) {
					a.move();
				}
				if (time % 100 == 1) {
					states.get(id++)[idx] = new AntState(id, time, (float)a.getXPos(), (float)a.getYPos(), (float)a.getDirection());
				}
			}
			
			if (time % 100 == 1)
				idx++;
		}
		
		return states;
	}

	private void findCollision(SimAnt ant) {
		Point p1, p2;
		p1 = new Point(ant.getHeadx(), ant.getHeady());
		ant.setCollision(false);

		// ant collision
		for (SimAnt a : ants) {
			if (a.equals(ant)) {
				continue;
			}

			p2 = new Point(a.getXPos(), a.getYPos());
			if (distance(p1, p2) > (ant.getHeadRadius() + ant.getRadius()))
				ant.setCollision(false);
			else
				ant.setCollision(true);
		}

		if (ant.getCollision())
			return;

		// wall collision
		if ((ant.getHeadx() + 6) >= 800 || (ant.getHeadx() - 6) <= 0
				|| (ant.getHeady() + 6) >= 600 || (ant.getHeady() - 6) <= 0)
			ant.setCollision(true);
	}

	private double distance(Point p1, Point p2) {
		return Math.sqrt(Math.pow((p1.x - p2.x), 2)
				+ Math.pow((p1.y - p2.y), 2));
	}

	public void writeFiles(TreeMap<Integer, AntState[]> map)
			throws FileNotFoundException {
		for (int i = 1; i <= map.keySet().size(); i++) {
			AntState[] current = map.get(i);
			String fileName = "simAnt" + i + ".txt";
			String fileName2 = "simAntD" + i + ".txt";

			Formatter writer = new Formatter(fileName);
			Formatter writer2 = new Formatter(fileName2);
			writer.format("1\n%d\n", i);
			writer2.format("1\n%d\n", i);

			for (int j = 0; j < current.length; j++) {
				writer.format("%d %.4f %.4f\n", current[j].timeStamp,
						current[j].x, current[j].y);
				writer2.format("%d %.4f %.4f %.4f\n", current[j].timeStamp,
						current[j].x, current[j].y, current[j].theta);
			}
			writer.close();
			writer2.close();
		}
	}

}
