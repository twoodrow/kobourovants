package ant_mysql;

public class AntState {

	// vars
	public final int antId;
	public final int timeStamp;
	public final float x, y, theta;
	public final String xStr, yStr, thetaStr;
	public final String data;
	
	/**
	 * Constructs from components.
	 * @param id
	 * @param time
	 * @param xPos
	 * @param yPos
	 * @param angle
	 */
	public AntState(int id, int time, float xPos, float yPos, float angle)
	{
		antId = id;
		timeStamp = time;
		x = xPos;
		y = yPos;
		theta = angle;
		
		xStr = String.format("%.4f", xPos);
		yStr = String.format("%.4f", yPos);
		thetaStr = String.format("%.4f", angle);
		data = id + "," + timeStamp + "," + x + "," + y + "," + theta + "\n";
	}
	
	/**
	 * Constructs from SQL Table 'AntState' line.
	 * @param line
	 */
	public AntState(String line)
	{
		data = line;		
		// split out for data elements
		String[] toParse = line.split(",");
		antId = Integer.parseInt(toParse[0]);
		timeStamp = Integer.parseInt(toParse[1]);
		xStr = toParse[2];
		yStr = toParse[3];	
		
		x = Float.parseFloat(xStr);
		y = Float.parseFloat(yStr);
		
		thetaStr = toParse[4];
		theta = Float.parseFloat(thetaStr);
	}
}
