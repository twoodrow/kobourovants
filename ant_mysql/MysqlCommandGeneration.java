package ant_mysql;

import java.io.FileNotFoundException;
import java.util.Formatter;

import org.junit.Test;

import ant_io.ParseGroundTruth;
import ant_mysql.SQLCoordinate;

public class MysqlCommandGeneration {

	@Test
	public void UpdateCoordinateTable() throws FileNotFoundException
	{
		Formatter writer = new Formatter("UpdateCoordinateTable.txt");
		SQLCoordinate[] coords = new SQLCoordinate[33];
		
		for(int i = 1; i < 33; i++)
		{
			coords[i-1] = ParseGroundTruth.getCoordinate("colony06-b_10k-GT-all.txt", 1, i);
			writer.format("update coordinate set x=%.3f,y=%.3f where pathId=%d;\n", coords[i-1].x, coords[i-1].y, 50+i);
		}
		
		writer.close();
	}
}
