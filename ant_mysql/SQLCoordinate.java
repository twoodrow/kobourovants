package ant_mysql;

public class SQLCoordinate {

	public final float x;
	public final float y;
	
	public SQLCoordinate(float xVal, float yVal)
	{
		x = xVal;
		y = yVal;
	}
}
