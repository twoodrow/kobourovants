package ant_base;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import ant_trajectories.GroundTruth;

public class AntGraph {

	// base collections
	ArrayList<AntNode> baseNodes;
	ArrayList<AntEdge> baseEdges;
	TreeMap<Integer, ArrayList<AntNode>> groundTruthPaths;
	String fileName;
	
	// the number of ant_ids in the file
	int numberAntsTracked;
	
	// user vars
	int numberUsersWalked = 0;
	int totalUserMistakes = 0;
	
	
	// feature collections
	// key is timestamp, value is list of the nodes with that timestamp
	private Map<Integer, ArrayList<AntNode>> nodesByTimestamp, nodesByInDegree;
	// key is node id, value is list of nodes with that edge incident to or from
	private Map<Integer, ArrayList<AntEdge>> edgesByIncidentFromNodeId, edgesByIncidentToNodeId;
	// nodes which do not have equal in and out degrees
	private ArrayList<AntNode> nodesWithInOutDegreeMismatch;
	
	/**
	 * Takes in nodes and edges base collections and generates feature collections
	 * @param nodes The collection sorted by nodeId of all nodes
	 * @param edges The unsorted collection of all edges
	 * @param numberAntIds The number of ants tracked which the data was generated from. So the number of nodes
	 * will be less than or equal to this number.
	 */
	public AntGraph(ArrayList<AntNode> nodes,
			ArrayList<AntEdge> edges, 
			TreeMap<Integer, ArrayList<AntNode>> groundTruth,
			int numberAntIds, 
			String filename)
	{
		fileName = filename;
		baseNodes = nodes;
		baseEdges = edges;
		numberAntsTracked = numberAntIds;
		groundTruthPaths = groundTruth;
		
		PartitionNodesByTimestamp();
		PartitionNodesByInDegree();
		PartitionEdgesByIncidentToAndFromNode();
		PartitionNodesByInOutDegreeMismatch();
		
		//groundTruthPaths = GroundTruth.create(filename);
		
		// testing
		// verified
		/*
		AntEdge test = edgesByIncidentFromNodeId.get(0).get(0);
		test.incrementAntTrajectory(0);
		
		int val1 = edgesByIncidentFromNodeId.get(0).get(0).GetNumTrajectoriesForAntId(0);
		int val2 = test.GetNumTrajectoriesForAntId(0);
		
		if(val1 != val2)
			System.out.println("mismatch");
		else
			System.out.println("match");
			*/
	}
	
	public AntGraph(ArrayList<AntNode> nodes, ArrayList<AntEdge> edges,  int numberAntIds, String filename)
	{
		fileName = filename;
		baseNodes = nodes;
		baseEdges = edges;
		numberAntsTracked = numberAntIds;
		
		PartitionNodesByTimestamp();
		PartitionNodesByInDegree();
		PartitionEdgesByIncidentToAndFromNode();
		PartitionNodesByInOutDegreeMismatch();
	}
	
	private void PartitionNodesByTimestamp()
	{
		nodesByTimestamp = new TreeMap<Integer, ArrayList<AntNode>>();
		
		// iterate over all nodes
		for(int i = 0; i < baseNodes.size(); i++)
		{
			AntNode node = baseNodes.get(i);
			
			if(nodesByTimestamp.containsKey(node.timeStamp))
			{
				nodesByTimestamp.get(node.timeStamp).add(node);
			}
			else
			{
				ArrayList<AntNode> list = new ArrayList<AntNode>();
				list.add(node);
				nodesByTimestamp.put(node.timeStamp, list);
			}
		}
	}
	private void PartitionNodesByInDegree()
	{
		nodesByInDegree = new TreeMap<Integer, ArrayList<AntNode>>();
		
		// iterate over all nodes
		for(int i = 0; i < baseNodes.size(); i++)
		{
			AntNode node = baseNodes.get(i);
			
			if(nodesByInDegree.containsKey(node.GetInDegree()))
			{
				nodesByInDegree.get(node.GetInDegree()).add(node);
			}
			else
			{
				ArrayList<AntNode> list = new ArrayList<AntNode>();
				list.add(node);
				nodesByInDegree.put(node.GetInDegree(), list);
			}
		}
	}
	private void PartitionEdgesByIncidentToAndFromNode()
	{
		edgesByIncidentFromNodeId = new TreeMap<Integer, ArrayList<AntEdge>>();
		edgesByIncidentToNodeId = new TreeMap<Integer, ArrayList<AntEdge>>();
		
		// iterate over all nodes
		for(int i = 0; i < baseEdges.size(); i++)
		{
			AntEdge edge = baseEdges.get(i);
			int from = edge.GetIncidentFromNodeId();
			int to = edge.GetIncidentToNodeId();
			
			// from node
			if(edgesByIncidentFromNodeId.containsKey(from))
				edgesByIncidentFromNodeId.get(from).add(edge);
			else
			{
				ArrayList<AntEdge> list = new ArrayList<AntEdge>();
				list.add(edge);
				edgesByIncidentFromNodeId.put(from, list);
			}
			
			// to node
			if(edgesByIncidentToNodeId.containsKey(to))
				edgesByIncidentToNodeId.get(to).add(edge);
			else
			{
				ArrayList<AntEdge> list = new ArrayList<AntEdge>();
				list.add(edge);
				edgesByIncidentToNodeId.put(to, list);
			}
		}
	}
	private void PartitionNodesByInOutDegreeMismatch()
	{
		nodesWithInOutDegreeMismatch = new ArrayList<AntNode>();
		for(int i = 0; i < baseNodes.size(); i++)
		{
			ArrayList<AntEdge> fromList = edgesByIncidentFromNodeId.get(i);
			ArrayList<AntEdge> toList = edgesByIncidentToNodeId.get(i);
			
			// check for starting and ending nodes
			if(toList == null || fromList == null)
				continue;
			
			if(toList.size() != fromList.size())
				nodesWithInOutDegreeMismatch.add(baseNodes.get(i));
		}
	}
	
	public Map<Integer, ArrayList<AntNode>> getNodesByTimestamp()
	{
		return nodesByTimestamp;
	}
	public Map<Integer, ArrayList<AntNode>> getNodesByInDegree()
	{
		return nodesByInDegree;
	}
	public TreeMap<Integer, ArrayList<AntEdge>> getEdgesByIncidentFromNodeId()
	{
		return (TreeMap<Integer, ArrayList<AntEdge>>) edgesByIncidentFromNodeId;
	}
	public TreeMap<Integer, ArrayList<AntEdge>> getEdgesByIncidentToNodeId()
	{
		return (TreeMap<Integer, ArrayList<AntEdge>>) edgesByIncidentToNodeId;
	}
	public ArrayList<AntNode> getBaseNodes()
	{
		return baseNodes;
	}
	public int getNumAnts()
	{
		return numberAntsTracked;
	}
	public int[] getValidAntIds()
	{
		ArrayList<AntNode> start = getNodesByTimestamp().get(1);
		int[][] validIds = new int[start.size()][];
		int i = 0;
		for(AntNode node : start)
			validIds[i++] = getAntIdsAtNode(node);
		
		int count = 0;
		for(int[] ids : validIds)
			count += ids.length;
		
		int[] ret = new int[count];
		i = 0;
		for(int[] ids : validIds)
			for(int id : ids)
				ret[i++] = id;
		
		return ret;
	}
	public int[] getAntIdsAtNode(AntNode node)
	{
		ArrayList<AntEdge> from = getEdgesByIncidentFromNodeId().get(node.nodeId);
		ArrayList<AntEdge> to = getEdgesByIncidentToNodeId().get(node.nodeId);
		ArrayList<Integer> tRet = new ArrayList<Integer>();
		
		if(from != null)
			for(AntEdge e : from)
				tRet.addAll(e.getAntIdsTraversing());
		
		if(to != null)
			for(AntEdge e : to)
				tRet.addAll(e.getAntIdsTraversing());
		
		// create return array
		int[] temp = new int[tRet.size()];
		int i = 0;
		for(Integer el : tRet)
			temp[i++] = el;
		
		Arrays.sort(temp);
		
		int[] ret = getUniqueElements(temp);
		
		return ret;	
	}
	public void performUserRandomWalk(int startAntId, float errorChance)
	{
		// vars
		Random rand = new Random();		
		// get ground truth path
		ArrayList<AntNode> currentTruthPath = groundTruthPaths.get(startAntId);
		int traverse = currentTruthPath.size() - 1;
		int currentId = -1;
		
		for(int i = 0; i < traverse; i++)
		{
			currentId = currentTruthPath.get(i).nodeId;
			
			// Special case: if two ants are travelling together, there will be a
			if(edgesByIncidentFromNodeId.get(currentId).size() > 1)
			{
				//check for mistake
				boolean mistake = (errorChance > rand.nextFloat());
				
				if(mistake)
				{
					// change truth path
					ArrayList<AntEdge> edges = edgesByIncidentFromNodeId.get(currentId);
					int nextId = currentTruthPath.get(i+1).nodeId;
					int swapId = -1;
					
					if(edges.get(0).GetIncidentToNodeId() == nextId)
						swapId = edges.get(1).GetIncidentToNodeId();
					else
						swapId = edges.get(0).GetIncidentToNodeId();
					
					currentTruthPath = findTruthPath(currentId, swapId);
					
					// increment total errors
					totalUserMistakes++;
				}
			}
			
			// mark edge visited
			AntEdge ae = findEdgeUsingIncidentIds(currentId, currentTruthPath.get(i+1).nodeId);
			//ae.incrementAntTrajectory(currentTruthPath.get(0).nodeId + 1);
			ae.incrementAntTrajectory(startAntId);
		}
		
		numberUsersWalked++;		
	}
	
	public AntNode[] performUserRandomWalk(int startAntId, double pathErrorChance, double clickPrecision)
	{
		// vars
		AntNode[] newPath;
		Random rand = new Random();		
		// get ground truth path
		ArrayList<AntNode> currentTruthPath = groundTruthPaths.get(startAntId);
		int traverse = currentTruthPath.size() - 1;
		newPath = new AntNode[traverse+1];
		AntNode temp = currentTruthPath.get(0);
		newPath[0] = new AntNode(1, temp.xPos, temp.yPos, startAntId);
		int currentId = -1;
		
		for(int i = 0; i < traverse; i++)
		{
			currentId = currentTruthPath.get(i).nodeId;
			
			// calculate attempted click node
			Double theta = rand.nextDouble() * 360;
			Double dist = rand.nextDouble() * clickPrecision;
			double dx = currentTruthPath.get(i).xPos + (dist * Math.cos(theta));
			double dy = currentTruthPath.get(i).yPos + (dist * Math.sin(theta));
			newPath[i+1] = new AntNode(1+(100*i), dx, dy, startAntId);
			
			// Special case: if two ants are travelling together, there will be a
			if(edgesByIncidentFromNodeId.get(currentId).size() > 1)
			{
				//check for mistake
				boolean mistake = (pathErrorChance > rand.nextDouble());
				
				if(mistake)
				{
					// change truth path
					ArrayList<AntEdge> edges = edgesByIncidentFromNodeId.get(currentId);
					int nextId = currentTruthPath.get(i+1).nodeId;
					int swapId = -1;
					
					if(edges.get(0).GetIncidentToNodeId() == nextId)
						swapId = edges.get(1).GetIncidentToNodeId();
					else
						swapId = edges.get(0).GetIncidentToNodeId();
					
					currentTruthPath = findTruthPath(currentId, swapId);
					
					// increment total errors
					totalUserMistakes++;
				}
			}
			
			// mark edge visited
			AntEdge ae = findEdgeUsingIncidentIds(currentId, currentTruthPath.get(i+1).nodeId);
			//ae.incrementAntTrajectory(currentTruthPath.get(0).nodeId + 1);
			ae.incrementAntTrajectory(startAntId);
		}
		
		numberUsersWalked++;
		return newPath;
	}
	
	public void computeGroundTruth()
	{
		groundTruthPaths = GroundTruth.create(fileName);
	}
	public TreeMap<Integer, ArrayList<AntNode>> getGroundTruthPaths()
	{
		return groundTruthPaths;
	}
	public void outputGraphToFile(String fileName) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
		// write nodes
		writer.write(baseNodes.size() + "\r\n");
		for(int i = 0; i < baseNodes.size(); i++)
			writer.write(baseNodes.get(i).toString() + "\r\n");
		
		// write edges
		writer.write(baseEdges.size() + "\r\n");
		for(int i = 0; i < baseEdges.size(); i++)
		{
			AntEdge ae = baseEdges.get(i);
			writer.write(ae.GetIncidentFromNodeId() + " " + ae.GetIncidentToNodeId());
			for(int j = 0; j < numberAntsTracked; j++)
				writer.write(" " + (j+1) + " " + ae.GetNumTrajectoriesForAntId(j+1));
			
			writer.write("\r\n");
		}
		
		// write ground truth
		//writer.write(""+numberAntsTracked);
		//writer.write("\r\n");
		
		
		// write statistics
		writer.write("Statistics: \r\n");
		writer.write("Number users walked: " + numberUsersWalked);
		writer.write("\r\n");
		writer.write("Total user mistakes: " + totalUserMistakes);
				
		writer.close();
	}
	
	public void outputGraphWithGroundTruthToFile(String fileName) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
		// write nodes as just timestamp
		writer.write(baseNodes.size() + "\r\n");
		for(int i = 0; i < baseNodes.size(); i++)
			writer.write(baseNodes.get(i).toString() + "\r\n");
		
		// write edges
		writer.write(baseEdges.size() + "\r\n");
		for(int i = 0; i < baseEdges.size(); i++)
		{
			AntEdge ae = baseEdges.get(i);
			writer.write(ae.GetIncidentFromNodeId() + " " + ae.GetIncidentToNodeId());
			writer.write("\r\n");
		}
		
		// write ground truth		
		for(int i = 0; i < numberAntsTracked; i++)
		{
			ArrayList<AntNode> path = groundTruthPaths.get(i);
			for(int j = 0; j < path.size(); j++)
				writer.write(path.get(j).nodeId+ " ");

			writer.write("\r\n");
		}
		
		writer.close();
	}
	
	public void outputGraphForAnalysisToFile(String fileName) throws IOException
	{
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
		// write nodes as just timestamp
		writer.write(baseNodes.size() + "\r\n");
		for(int i = 0; i < baseNodes.size(); i++)
		{
			writer.write(baseNodes.get(i).timeStamp / 100 + "\r\n");
		}
		
		// write edges
		writer.write(baseEdges.size() + "\r\n");
		for(int i = 0; i < baseEdges.size(); i++)
		{
			AntEdge ae = baseEdges.get(i);
			writer.write(ae.GetIncidentFromNodeId() + " " + ae.GetIncidentToNodeId());
			for(int j = 0; j < numberAntsTracked; j++)
			{
				int val = ae.GetNumTrajectoriesForAntId(j);
				writer.write(" " + val);
			}
			writer.write("\r\n");
		}
		
		// ground truth
		for(int i = 0; i < numberAntsTracked; i++)
		{
			ArrayList<AntNode> path = groundTruthPaths.get(i);
			//writer.write(""+(i+1));
			for(int j = 0; j < path.size(); j++)
				writer.write(path.get(j).nodeId+ " ");

			writer.write("\r\n");
		}
		
		/*
		// write statistics
		writer.write("Statistics: \r\n");
		writer.write("Number users walked: " + numberUsersWalked);
		writer.write("\r\n");
		writer.write("Total user mistakes: " + totalUserMistakes);
		*/
		
		writer.close();
	}
	
	private AntEdge findEdgeUsingIncidentIds(int fromId, int toId)
	{
		AntEdge toReturn = null;
		
		// get both edge collections and check for the edge that is contained in both lists
		ArrayList<AntEdge> from, to;
		from = edgesByIncidentFromNodeId.get(fromId);
		to = edgesByIncidentToNodeId.get(toId);
		
		for(AntEdge ae : from)
		{
			if(to.contains(ae))
			{
				toReturn = ae;
				break;
			}
		}
		
		if(toReturn == null)
		{
			System.err.println("findEdgeUsingIncidentIds got null.");
			System.exit(1);
		}
		
		return toReturn;
	}
	private ArrayList<AntNode> findTruthPath(int currentId, int nextId)
	{
		// search all groundTruth paths
		for(int i = 0; i < numberAntsTracked; i++)
		{
			ArrayList<AntNode> an = groundTruthPaths.get(i);
			for(int j = 0; j < an.size(); j++)
				if(an.get(j).nodeId == currentId)
				{
					if(an.get(j+1).nodeId == nextId)
						return groundTruthPaths.get(i);
				}		
		}
		
		return null;
	}
	
	public static void writeAntPathToFile(String outFile, int antId, int videoId, AntNode[] nodes) throws FileNotFoundException
	{
		Formatter writer = null;
		writer = new Formatter(outFile);
		
		writer.format("%d\n", videoId);
		writer.format("%d\n", antId);
		
		for(int i = 0; i < nodes.length; i++)
			writer.format("%d  %.4f %.4f\n", nodes[i].timeStamp, nodes[i].xPos, nodes[i].yPos);
				
		writer.close();
	}
	
	public static int[] getUniqueElements(int[] arr)
	{
		int[] temp = new int[arr.length];
		int i = 0;
		temp[i] = arr[i];
		for(int idx = 1; idx < arr.length; idx++)
		{
			// check dupe
			int val = arr[idx];
			if(val == temp[i])
				continue;
			else
			{
				i++;
				temp[i] = val;
			}
		}
		
		int[] ret = new int[i+1];
		for(int j = 0; j < i+1; j++)
			ret[j] = temp[j];
		
		return ret;
	}
}
