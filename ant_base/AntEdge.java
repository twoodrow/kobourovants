package ant_base;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class AntEdge {

	// IDs of nodes that the edge is connected to
	private int idNodeIncidentFrom, idNodeIncidentTo;
	// corresponds to the number of trajectories tracked for this edge
	// index is the ant_id
	private Map<Integer, Integer> weights = new TreeMap<Integer, Integer>();
	private String input;
	
	/**
	 * Constructs an AntEdge for AntGraph
	 * @param fromId The ID of the node the edge is coming from
	 * @param toId The ID of the node the edge is going to
	 * @param antTrajectories Collection containing number of trajectories tracked for a specific ant walking this edge
	 */
	public AntEdge(String line) {
		input = line;
		String[] data = line.split("\\s");
		idNodeIncidentFrom = Integer.valueOf(data[0]);
		idNodeIncidentTo = Integer.valueOf(data[1]);
		
		for(int i = 2; i < data.length; i+=2)
		{
			if(Integer.valueOf(data[i+1]) != 0)
			{
				int id = Integer.valueOf(data[i]);
				int weight = Integer.valueOf(data[i+1]);
				weights.put(id, weight);
			}
		}
	}
	
	
	// get methods
	public int GetIncidentFromNodeId() { return idNodeIncidentFrom; }
	public int GetIncidentToNodeId() { return idNodeIncidentTo; }
	public int GetNumTrajectoriesForAntId(int antId)
	{
		int edgeWeight = 0;
		
		if(weights.containsKey(antId))
			edgeWeight = weights.get(antId);
		
		return edgeWeight;
	}
	public void incrementAntTrajectory(int antId)
	{
		int val = GetNumTrajectoriesForAntId(antId) + 1;
		weights.put(antId, val);
	}
	public Set<Integer> getAntIdsTraversing()
	{
		return weights.keySet();
	}
	/**
	 * Returns the string data that this edge was generated from.
	 */
	public String toString(){
		return input;
	}
}
