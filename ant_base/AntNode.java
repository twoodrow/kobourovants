package ant_base;

public class AntNode {
	public final double xPos;
	public final double yPos;
	public final int timeStamp;
	public final int nodeId;
	private int inDegree;
	private int outDegree;

	public AntNode(String line, int id) {
		String[] values = line.split("\\s");
		timeStamp = Integer.valueOf(values[0]);
		xPos = Double.valueOf(values[1]);
		yPos = Double.valueOf(values[2]);
		nodeId = id;
	}
	
	public AntNode(int time, double xPosition, double yPosition, int id) {
		timeStamp = time;
		xPos = xPosition;
		yPos = yPosition;
		nodeId = id;
	}
	
	public void IncrementInDegree() {
		inDegree++;
	}
	
	public void IncrementOutDegree() {
		outDegree++;
	}
	
	public String toString(){
		String result = "";
		result += timeStamp;
		result += " " + xPos + " " + yPos;
		return result;
	}
	
	// get methods
	public int GetInDegree() { return inDegree; }
	public int GetOutDegree() { return outDegree; }
}
