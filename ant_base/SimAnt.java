package ant_base;

import java.util.ArrayList;
import java.util.Random;

public class SimAnt {

	private class Point {
		private double x, y;

		public Point(double xp, double yp) {
			x = xp;
			y = yp;
		}
	}

	private double xPos;
	private double yPos;
	private double radius;
	private double spec;
	private double headRadius;
	private double direction;
	private double speed;
	private Random rand;
	private boolean collision;
	private String lastTurn;
	private ArrayList<Point> points;

	public SimAnt(double specialization, int id) {
		rand = new Random();
		xPos = rand.nextInt(800) + rand.nextDouble();
		yPos = rand.nextInt(600) + rand.nextDouble();
		radius = 10;
		spec = specialization;
		headRadius = 6;
		speed = 3 * (1 - Math.sqrt(specialization));
		if(id<5)
			speed = 6 * (1 - Math.sqrt(specialization));
		direction = rand.nextInt(360);
		collision = false;
		lastTurn = "";
		points = new ArrayList<Point>();
		points.add(new Point(200, 400));
		points.add(new Point(400, 100));
		points.add(new Point(750, 550));
		points.add(new Point(700, 300));
		points.add(new Point(700, 200));
		points.add(new Point(600, 400));
		points.add(new Point(550, 100));
		points.add(new Point(100, 550));
		points.add(new Point(100, 100));
	}

	public void turn() {
		boolean biased = rand.nextBoolean();
		int u, b;
		if (biased) {
			b = 1;
			u = -1;
		} else {
			b = -1;
			u = 1;
		}

		double tu, tb;
		tu = 30;
		tb = (1-spec) * 90;

		Point p = findPoint();

		double angleToCenter;
		double distance = Math.sqrt(Math.pow((xPos - p.x), 2)
				+ Math.pow((yPos - p.y), 2));

		angleToCenter = Math.toDegrees(Math.acos((p.x - xPos) / distance));

		double a2 = Math.toDegrees(Math.asin((p.y - yPos) / distance));

		if (xPos > p.x)
			a2 = 180 - a2;
		if (a2 < 0)
			a2 += 360;

		if (yPos > p.y)
			angleToCenter = 360 - angleToCenter;

		angleToCenter = Math.abs(angleToCenter - direction);
		a2 = Math.abs(a2 - direction);

		if (angleToCenter > 180)
			angleToCenter = 360 - angleToCenter;
		if (a2 > 180)
			a2 = 360 - a2;

		// System.out.println(direction + " "+xPos+", "+yPos+" : "+angleToCenter
		// + " " + a2);

		double result = tu * u + tb * b
				* ((1 - Math.cos(Math.toRadians(angleToCenter))) / 2);

		direction += result;
		if (direction >= 360)
			direction -= 360;
		if (direction < 0)
			direction += 360;
	}

	private void turnLeft() {
		lastTurn = "left";

		boolean biased = rand.nextBoolean();
		int u, b;
		if (biased) {
			b = 1;
			u = -1;
		} else {
			b = -1;
			u = 1;
		}

		double tu, tb;
		tu = 30;
		tb = (1 - spec) * 90;

		double angleToCenter;
		double distance = Math.sqrt(Math.pow((xPos - 400), 2)
				+ Math.pow((yPos - 300), 2));

		angleToCenter = Math.toDegrees(Math.acos((400 - xPos) / distance));

		double a2 = Math.toDegrees(Math.asin((300 - yPos) / distance));

		if (xPos > 400)
			a2 = 180 - a2;
		if (a2 < 0)
			a2 += 360;

		if (yPos > 300)
			angleToCenter = 360 - angleToCenter;

		angleToCenter = Math.abs(angleToCenter - direction);
		a2 = Math.abs(a2 - direction);

		if (angleToCenter > 180)
			angleToCenter = 360 - angleToCenter;
		if (a2 > 180)
			a2 = 360 - a2;

		// System.out.println(direction + " "+xPos+", "+yPos+" : "+angleToCenter
		// + " " + a2);

		double result = tu * u + tb * b
				* ((1 - Math.cos(Math.toRadians(angleToCenter))) / 2);

		result = Math.abs(result);

		direction -= result;
		if (direction >= 360)
			direction -= 360;
		if (direction < 0)
			direction += 360;
	}

	private void turnRight() {
		lastTurn = "right";

		boolean biased = rand.nextBoolean();
		int u, b;
		if (biased) {
			b = 1;
			u = -1;
		} else {
			b = -1;
			u = 1;
		}

		double tu, tb;
		tu = 30;
		tb = (1 - spec) * 90;

		double angleToCenter;
		double distance = Math.sqrt(Math.pow((xPos - 400), 2)
				+ Math.pow((yPos - 300), 2));

		angleToCenter = Math.toDegrees(Math.acos((400 - xPos) / distance));

		double a2 = Math.toDegrees(Math.asin((300 - yPos) / distance));

		if (xPos > 400)
			a2 = 180 - a2;
		if (a2 < 0)
			a2 += 360;

		if (yPos > 300)
			angleToCenter = 360 - angleToCenter;

		angleToCenter = Math.abs(angleToCenter - direction);
		a2 = Math.abs(a2 - direction);

		if (angleToCenter > 180)
			angleToCenter = 360 - angleToCenter;
		if (a2 > 180)
			a2 = 360 - a2;

		// System.out.println(direction + " "+xPos+", "+yPos+" : "+angleToCenter
		// + " " + a2);

		double result = tu * u + tb * b
				* ((1 - Math.cos(Math.toRadians(angleToCenter))) / 2);

		result = Math.abs(result);

		direction += result;
		if (direction >= 360)
			direction -= 360;
		if (direction < 0)
			direction += 360;
	}

	public void uTurn() {
		direction += 180;
		if (direction >= 360)
			direction -= 360;
		if (direction < 0)
			direction += 360;
	}

	public void move() {
		xPos += speed * Math.cos(Math.toRadians(direction));
		yPos += speed * Math.sin(Math.toRadians(direction));
	}

	private double distance(Point p1, Point p2) {
		return Math.sqrt(Math.pow((p1.x - p2.x), 2)
				+ Math.pow((p1.y - p2.y), 2));
	}

	private Point findPoint() {
		Point result = null;
		Point ant = new Point(xPos, yPos);
		double dist = 10000;
		for (Point p : points) {
			if (distance(p, ant) < dist) {
				dist = distance(p, ant);
				result = p;
			}
		}
		return result;
	}

	public double getXPos() {
		return xPos;
	}

	public double getYPos() {
		return yPos;
	}

	public boolean getCollision() {
		return collision;
	}

	public void setCollision(boolean value) {
		collision = value;
	}

	public double getRadius() {
		return radius;
	}

	public double getHeadRadius() {
		return headRadius;
	}

	public double getHeadx() {
		return xPos + radius * Math.cos(Math.toRadians(direction));
	}

	public double getHeady() {
		return yPos + radius * Math.sin(Math.toRadians(direction));
	}

	public double getDirection() {
		return direction;
	}

}
