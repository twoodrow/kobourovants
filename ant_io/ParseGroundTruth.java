package ant_io;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;
import java.util.TreeMap;

import org.junit.Test;

import ant_mysql.AntState;
import ant_mysql.SQLCoordinate;
import ant_statistics.Statistics;

public class ParseGroundTruth {
	
	private ParseGroundTruth(){};

	public static SQLCoordinate getCoordinate(String fileName, int timeStamp, int antId) throws FileNotFoundException
	{
		// vars
		boolean foundLine = false;
		String[] data = null;
		Scanner reader = new Scanner(new FileInputStream(fileName));
		
		while(reader.hasNextLine() && !foundLine)
		{
			String line = reader.nextLine();
			data = line.split(",");
			
			// ensure this is the correct data entry
			if(Integer.parseInt(data[0]) != antId)
				continue;
			else if(Integer.parseInt(data[1]) != timeStamp)
				continue;
			else
				foundLine = true;
		}
		
		reader.close();
		
		if(!foundLine)
			return null;
		
		Float x = Float.parseFloat(data[2]);
		Float y = Float.parseFloat(data[3]);
		return new SQLCoordinate(x,y);
	}
	
	/**
	 * Reads a file containing ground truth data for ants. The entire should only contain lines in this format:
	 * <p>
	 * <code>antId(int),timestamp(int), x position(float), y position(float), theta angle(float)</code>
	 * ***ANTS CONTAINING POINTS OFFSCREEN(x and y are -1) WILL NOT BE INCLUDED***
	 * @param fileName
	 * @return TreeMap whose keys are the AntIds and values are arrays AntStates sorted by timestamp
	 * @throws FileNotFoundException
	 */
	public static TreeMap<Integer, AntState[]> readEntireGroundTruthFile(String fileName) throws FileNotFoundException
	{
		// vars
		TreeMap<Integer, ArrayList<AntState>> states = new TreeMap<Integer, ArrayList<AntState>>();
		Scanner reader = new Scanner(new FileInputStream(fileName));
		
		// read file
		while(reader.hasNextLine())
		{
			String line = reader.nextLine();
			AntState as = new AntState(line);
			
			if(!states.containsKey(as.antId))
				states.put(as.antId, new ArrayList<AntState>());
			
			states.get(as.antId).add(as);
		}
		
		// done reading
		reader.close();
		
		// build array-based tree set
		TreeMap<Integer, AntState[]> colony = new TreeMap<Integer, AntState[]>();
		for(int key : states.keySet())
		{
			ArrayList<AntState> list = states.get(key);
			AntState[] ant = new AntState[list.size()];
			for(int i = 0; i < ant.length; i++)
				ant[i] = list.get(i);
			colony.put(key, ant);
		}
		
		return colony;
	}
	
	/**
	 * Assumes colonies contain data for individual ants only; no colony totals.
	 * @param colonies
	 * @return
	 */
	public static TreeMap<Integer, AntState[]> combineColonies(TreeMap<Integer, AntState[]>[] colonies)
	{
		// vars
		TreeMap<Integer, AntState[]> ret = new TreeMap<Integer, AntState[]>();
		int counter = 0;
		
		for(TreeMap<Integer, AntState[]> c : colonies)
		{
			for(int i : c.keySet())
				ret.put(counter++, c.get(i));
		}
		
		return ret;
	}
	
	@Test
	public static void ConvertGroundTruthHelper() throws FileNotFoundException
	{
		// file specific variables
		String fileName = "colony06-b_10k-GT-all.txt";
		String newFileName = "colony6_splitGT.txt";
		int maxAntId = 31;
		// timestamp configuration		
		int[] timeStamps = new int[101];
		timeStamps[0] = 1; // will be 1 in output
		for(int i = 1; i <= 100; i++)
			timeStamps[i] = 100*i;
		
		// method vars
		Formatter writer = new Formatter(new FileOutputStream(newFileName));	
		TreeMap<Integer, AntState[]> states = readEntireGroundTruthFile(fileName);
		
		// iterate through data
		for(int antId = 1; antId <= maxAntId; antId++)
		{
			int index = 0;
			AntState[] antStates = states.get(antId);
			
			for(int i = 0; i < antStates.length; i++)
			{
				if(antStates[i].timeStamp != timeStamps[index])
					continue;
				else
				{
					writer.format(antStates[i].data + "\n");
					index++;
				}
			}
		}
		
		writer.close();
	}
	
	@Test
	public static void TestOutputGroundTruthColonyData() throws FileNotFoundException
	{
		double biggest5=0, smallest5=Double.MAX_VALUE;
		double biggest6=0, smallest6=Double.MAX_VALUE;
		TreeMap<Integer, AntState[]> states5 = readEntireGroundTruthFile("groundtruth1\\colony5_splitGT.txt");
		TreeMap<Integer, AntState[]> states6 = readEntireGroundTruthFile("groundtruth1\\colony6_splitGT.txt");
		
		for(int i = 2; i < states6.size(); i++)
			assertTrue(states6.get(i).length == states6.get(i+1).length);
		for(int i = 1; i < states5.size(); i++)
			assertTrue(states5.get(i).length == states5.get(i+1).length);

		for(int id = 2; id < states6.size(); id++)
		{
			AntState[] ant = states6.get(id);
			double runningTotal = 0;
			for(int t = 2; t < ant.length-1; t++)
				runningTotal += Statistics.distance(ant[t].x, ant[t].y, ant[t+1].x, ant[t+1].y);
			
			// check and assign
			smallest6 = (runningTotal < smallest6)?  runningTotal : smallest6;
			biggest6 = (runningTotal > biggest6)? runningTotal : biggest6;
		}
		
		for(int id = 1; id < states5.size(); id++)
		{
			AntState[] ant = states5.get(id);
			double runningTotal = 0;
			for(int t = 1; t < ant.length-1; t++)
				runningTotal += Statistics.distance(ant[t].x, ant[t].y, ant[t+1].x, ant[t+1].y);

			// check and assign
			smallest5 = (runningTotal < smallest5)?  runningTotal : smallest5;
			biggest5 = (runningTotal > biggest5)? runningTotal: biggest5;
		}
		
		// output largest
		System.out.println("Colony6\nBiggest="+biggest6+"\nSmallest="+smallest6+"\n");
		System.out.println("Colony5\nBiggest="+biggest5+"\nSmallest="+smallest5+"\n");
	}
	
	public static TreeMap<Integer, AntState[]> getColony05() throws FileNotFoundException
	{
		return readEntireGroundTruthFile("groundtruth1\\colony5_splitGT.txt");
	}
	
	public static TreeMap<Integer, AntState[]> getColony06() throws FileNotFoundException
	{
		return readEntireGroundTruthFile("groundtruth1\\colony6_splitGT.txt");
	}
	
	public static TreeMap<Integer, AntState[]> getColony5Dec2013() throws FileNotFoundException
	{
		TreeMap<Integer, AntState[]>[] files = new TreeMap[5];
		for(int i = 0; i < files.length; i++)
			files[i] = readEntireGroundTruthFile("groundtruth1\\5Dec2013_sim" + (i+1) + ".txt");
		
		
		return combineColonies(files);
	}
}
