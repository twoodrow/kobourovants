package ant_io;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;

import ant_base.AntEdge;
import ant_base.AntGraph;
import ant_base.AntNode;

public class GraphReader {

	public static AntGraph ReadBigGraph(String filename)
	{
		// attempt to open file
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
			System.err.println("The file was not found.");
			e.printStackTrace();
			System.exit(1);
		}
		
		// check valid file
		if(fis == null)
		{
			System.err.println("Opening file failed.");
			System.exit(1);
		}
		// vars to parse file and load graph
		Scanner scanner = new Scanner(fis);
		ArrayList<AntNode> nodes = new ArrayList<AntNode>();
		ArrayList<AntEdge> edges = new ArrayList<AntEdge>();
		
		// get number of nodes to read
		int numNodes = Integer.valueOf(scanner.nextLine());
		// read nodes
		for(int i = 0; i < numNodes; i++)
		{
			// i corresponds to the node id
			if(scanner.hasNextLine())
			{
				String nodeData = scanner.nextLine();
				nodes.add(new AntNode(nodeData, i));
			}
			else
			{
				// file terminated early
				System.err.println(
						"The file ended before the number of nodes was read.\n" +
						"Check file format.");
				System.exit(1);
			}
		}
		
		// get number of edges to read
		int numEdges = Integer.valueOf(scanner.nextLine());
		int numAntIds = 0;
		// read edges
		for(int i = 0; i < numEdges; i++)
		{
			if(scanner.hasNextLine())
			{
				String edgeData = scanner.nextLine();
				edges.add(new AntEdge(edgeData));
				String[] nums = edgeData.split("\\s");
				nodes.get(Integer.valueOf(nums[0])).IncrementOutDegree();
				nodes.get(Integer.valueOf(nums[1])).IncrementInDegree();
				if(i==0)
					numAntIds = (nums.length-2)/2;
				if(numAntIds != (nums.length-2)/2) {
					System.err.println("Number of ants is not consistent.");
					System.exit(1);
				}			
			}
			else
			{
				System.err.println("Error: input file does not have the number of edges specified.");
				System.exit(1);
			}
		}
		
		scanner.close();
		
		return new AntGraph(nodes, edges, numAntIds, filename);
	}
	
	public static AntGraph ReadBigGraphWithGroundTruth(String filename)
	{
		// attempt to open file
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
			System.err.println("The file was not found.");
			e.printStackTrace();
			System.exit(1);
		}
		
		// check valid file
		if(fis == null)
		{
			System.err.println("Opening file failed.");
			System.exit(1);
		}
		// vars to parse file and load graph
		Scanner scanner = new Scanner(fis);
		ArrayList<AntNode> nodes = new ArrayList<AntNode>();
		ArrayList<AntEdge> edges = new ArrayList<AntEdge>();
		
		// get number of nodes to read
		int numNodes = Integer.valueOf(scanner.nextLine());
		// read nodes
		for(int i = 0; i < numNodes; i++)
		{
			// i corresponds to the node id
			if(scanner.hasNextLine())
			{
				String nodeData = scanner.nextLine();
				nodes.add(new AntNode(nodeData, i));
			}
			else
			{
				// file terminated early
				System.err.println(
						"The file ended before the number of nodes was read.\n" +
						"Check file format.");
				System.exit(1);
			}
		}
		
		// get number of edges to read
		int numEdges = Integer.valueOf(scanner.nextLine());
		int numAntIds = 50;
		// read edges
		for(int i = 0; i < numEdges; i++)
		{
			if(scanner.hasNextLine())
			{
				String edgeData = scanner.nextLine();
				edges.add(new AntEdge(edgeData));
				String[] nums = edgeData.split("\\s");
				nodes.get(Integer.valueOf(nums[0])).IncrementOutDegree();
				nodes.get(Integer.valueOf(nums[1])).IncrementInDegree();
				if(i==0)
					numAntIds = (nums.length-2)/2;
				if(numAntIds != (nums.length-2)/2) {
					System.err.println("Number of ants is not consistent.");
					System.exit(1);
				}			
			}
			else
			{
				System.err.println("Error: input file does not have the number of edges specified.");
				System.exit(1);
			}
		}
		
		// read ground truth
		TreeMap<Integer, ArrayList<AntNode>> gt = new TreeMap<Integer, ArrayList<AntNode>>();
		for(int i = 0; i < 50; i++)
		{
			ArrayList<AntNode> truthPath = new ArrayList<AntNode>();
			if(scanner.hasNextLine())
			{
				String gtData = scanner.nextLine();
				String[] nums = gtData.split("\\s");
				
				for(String nodeId : nums)
					truthPath.add(nodes.get(Integer.valueOf(nodeId)));
			}
			else
			{
				System.err.println("Error: input file does not have the number of ground truth paths specified.");
				System.exit(1);
			}
			
			gt.put(i, truthPath);
		}

		scanner.close();
		
		return new AntGraph(nodes, edges, gt, 50, filename);
	}
	
	public static void BigGraphFileToVizFile(String inputFilename, String outputFilename) throws IOException
	{
		// attempt to open file
		FileInputStream fis = null;
		BufferedWriter writer = null;
		
		try {
			writer = new BufferedWriter(new FileWriter(outputFilename));
		} catch (IOException e1) {
			System.err.println("Output file creation failed.");
			e1.printStackTrace();
			System.exit(1);
		}
		
		// input
		try {
			fis = new FileInputStream(inputFilename);
		} catch (FileNotFoundException e) {
			System.err.println("The file was not found.");
			e.printStackTrace();
			System.exit(1);
		}		

		// ouput
		
		// check valid file
		if(fis == null)
		{
			System.err.println("Opening input file failed.");
			System.exit(1);
		}
		
		// vars to parse file and load graph
		Scanner scanner = new Scanner(fis);
		
		// get number of nodes to read
		int numNodes = Integer.valueOf(scanner.nextLine());
		
		writer.write("digraph gr {\r\n\t");
		writer.write("ranksep=.75; size = \"7.5,7.5\"; rankdir=LR;\r\n");
		
		int time = -1;
		boolean first = true;
		
		// read nodes
		for(int i = 0; i < numNodes; i++)
		{
			// i corresponds to the node id
			if(scanner.hasNextLine())
			{
				String line = scanner.nextLine();
				String[] items = line.split("\\s");
				
				if(time != Integer.parseInt(items[0]))
				{
					if(!first)
					{
						// close rank
						writer.write(" }");
					}
					writer.write("\r\n\t { rank = same; ");
					time = Integer.parseInt(items[0]);
					first = false;
				}
				
				writer.write("\"" + i + "\";");
			}
			else
			{
				// file terminated early
				System.err.println(
						"The file ended before the number of nodes was read.\n" +
						"Check file format.");
				System.exit(1);
			}
		}
		

		// close last rank
		writer.write(" }");		

		// get number of edges to read
		int numEdges = Integer.valueOf(scanner.nextLine());
		
		// read edges
		for(int i = 0; i < numEdges; i++)
		{
			if(scanner.hasNextLine())
			{
				String edgeData = scanner.nextLine();
				String[] nums = edgeData.split("\\s");
				
				writer.write("\"" + nums[0] + "\"" + " -> " + "\"" + nums[1] + "\";\r\n");
			}
			else
			{
				System.err.println("Error: input file does not have the number of edges specified.");
				System.exit(1);
			}
				
		}
		// close graph
		writer.write(" }");	
		
		scanner.close();
		writer.close();
	}

}