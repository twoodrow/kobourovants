package ant_io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import ant_base.AntGraph;
import ant_base.AntNode;
import ant_statistics.Histogram;
import ant_statistics.Statistics;

public class GraphWriter2 {

	private AntGraph graph;

	public GraphWriter2(AntGraph g) {
		graph = g;
	}

	public void writeGraph(String fileName) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		int[] probabilities = Statistics.ExtractNodeProbabilities(graph);
		Histogram h = new Histogram(probabilities);

		int sum = 0; // total number of nodes
		int[] numAntGroups = new int[graph.getNodesByTimestamp().size()];
		numAntGroups[0] = graph.getNumAnts(); // always k ants at first
		sum += graph.getNumAnts();

		// sum all nodes
		for (int i = 1; i < numAntGroups.length; i++) {
			numAntGroups[i] = (int)h.GetRandomValue();
			sum += numAntGroups[i];
		}

		writer.write("" + sum + "\n");
		ArrayList<AntNode> nodes = new ArrayList<AntNode>();
		int timestamp = 1;
		int id = 0;
		Random rand = new Random();
		ArrayList<AntNode> inputNodes = graph.getBaseNodes();

		for (int i = 0; i < numAntGroups.length; i++) {
			int temp = numAntGroups[i];
			if (i != 0)
				timestamp += 100;
			for (int j = 0; j < temp; j++) {
				double xPos = inputNodes.get(rand.nextInt(inputNodes.size()))
						.xPos;
				double yPos = inputNodes.get(rand.nextInt(inputNodes.size()))
						.yPos;
				AntNode node = new AntNode(timestamp, xPos, yPos, id);
				id++;
				nodes.add(node);
			}
		}

//		for (AntNode n : nodes)
//			writer.write(n.toString() + "\n");

		int[] degrees = Statistics.FindNodeDegrees(graph);
		degrees[0] = 0; // don't care about nodes with in degree <2
		degrees[1] = 0;

		h = new Histogram(degrees);
		writer.write("5000\n");
		timestamp = 1;

		for (int i = 0; i < numAntGroups.length-1; i++) {
			if (i != 0)
				timestamp += 100;
			ArrayList<AntNode> prev = new ArrayList<AntNode>();
			ArrayList<AntNode> prevUsed = new ArrayList<AntNode>();
			ArrayList<AntNode> next = new ArrayList<AntNode>();
			ArrayList<AntNode> nextUsed = new ArrayList<AntNode>();

			for (AntNode n : nodes) {
				if (n.timeStamp == timestamp) {
					prev.add(n);
					if (n.GetInDegree() > 1)
						for (int j = 1; j < n.GetInDegree(); j++)
							prev.add(n);
				} else if (n.timeStamp == (timestamp + 100))
					next.add(n);
			}
			System.out.println(""+prev.size());
			int randDegree = 0;
			AntNode destination = null;

			for (AntNode n : prev) {
				n.IncrementOutDegree();
				prevUsed.add(n);

				if (next.size() == 0 && randDegree == 0) {
					randDegree = (int)h.GetRandomValue();

					while ((randDegree - 2 + prevUsed.size()) > prev.size())
						randDegree = (int)h.GetRandomValue();
					int randNum = rand.nextInt(nextUsed.size());
					destination = nextUsed.get(randNum);	
					nextUsed.remove(destination);
					writer.write(n.nodeId + " -> " + destination.nodeId
							+ ";\n");
					randDegree -= 2;
					destination.IncrementInDegree();

				} else if (next.size() == 0 && randDegree != 0) {
					writer.write(n.nodeId + " -> " + destination.nodeId
							+ ";\n");
					randDegree -= 1;
					destination.IncrementInDegree();

				} else {
					int randNum = rand.nextInt(next.size());
					destination = next.get(randNum);
					destination.IncrementInDegree();
					nextUsed.add(destination);
					next.remove(destination);
					writer.write(n.nodeId + " -> " + destination.nodeId
							+ ";\n");
				}
			}

		}
		writer.close();
	}
}
