package ant_io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Designed to split the ground truth files into individual ant files.
 * Format expected is: ant_id, frame_index, x_location, y_location, angle
 * @author Travis
 *
 */
public class GroundTruthFileExtractor {

	public static void extractByAntId(String inputFileName, String outputFileName, int antId, int videoId, int frequency)
	{
		// vars
		Formatter writer = null;
		FileInputStream fis = null;
		String[] currentLine = null;
		ArrayList<String[]> antLines = new ArrayList<String[]>();
		
		try {
			fis = new FileInputStream(inputFileName);
			writer = new Formatter(outputFileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Scanner reader = new Scanner(fis);
		
		// data is sorted by ant id
		// so scan to reach ant
		boolean madeItToAnt = false;		
		while(!madeItToAnt)
		{
			currentLine = reader.nextLine().split(",");
			madeItToAnt = (antId == Integer.valueOf(currentLine[0]));
		}
		
		// read until end of file or encounter new ant id
		boolean newAntEncountered = false;
		while(reader.hasNext() && !newAntEncountered)
		{
			antLines.add(currentLine);
			currentLine = reader.nextLine().split(",");
			newAntEncountered = (antId != Integer.valueOf(currentLine[0]));
		}
		
		// now parse data and write to file
		writer.format("%d\n%d\n", videoId, antId);
		int current = 1;
		while(current < antLines.size())
		{
			currentLine = antLines.get(current-1);
			writer.format("%d %.4f %.4f\n", Integer.valueOf(currentLine[1]), Float.valueOf(currentLine[2]), Float.valueOf(currentLine[3]));
			current += frequency;
		}
		
		writer.close();
		reader.close();
	}
}
